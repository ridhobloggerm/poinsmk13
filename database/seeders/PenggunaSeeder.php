<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class PenggunaSeeder extends Seeder
{
    public function run()
    {
        \App\Models\Pengguna::factory()->count(1)->create();
    }
}
