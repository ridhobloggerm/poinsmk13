<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use App\Models\Pengguna;
use Faker\Factory as Faker;
class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(PenggunaSeeder::class);
        $this->call(PelanggaranSeeder::class);
        $this->call(SiswaSeeder::class);
        $this->call(GuruSeeder::class);
        $this->call(TransaksiPelanggaranSeeder::class);
    }
}
