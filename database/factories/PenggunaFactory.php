<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class PenggunaFactory extends Factory
{
    public function definition()
    {
        return [
            'username' => 'admin',
            'password' => 'admin',
            'level' => 'admin',
        ];
    }
}
