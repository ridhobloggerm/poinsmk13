<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    
    public function up()
    {
        Schema::create('pelanggarans', function (Blueprint $table) {
            $table->id();
            $table->string('nama_pelanggaran');
            $table->bigInteger('jumlah_poin');
            $table->string('tindakan_langsung');
            $table->timestamps();
        });
    }

    
    public function down()
    {
        Schema::dropIfExists('pelanggarans');
    }
};
