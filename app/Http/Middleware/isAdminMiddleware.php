<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class isAdminMiddleware
{
    public function handle(Request $request, Closure $next)
    {
        if($request->session()->get('level') == "admin"){
            return $next($request);
        }else{
            return redirect()->route('auth.index')->with('warning','Login terlebih dahulu!');
        }
    }
}
