<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pelanggaran;
use App\Models\TransaksiPelanggaran;
use App\Models\Pengguna;
use App\Models\Guru;
use App\Models\Siswa;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    public function __construct(){
        return $this->middleware('isAdmin');
    }

    public function index(Request $request){
        $pengguna = \App\Models\Pengguna::count();
        $guru = \App\Models\Guru::count();
        $siswa = \App\Models\Siswa::count();

        return view('admin.index', ['pengguna' => $pengguna, 'guru' => $guru, 'siswa' => $siswa]);
    }

    public function createpelanggaran(){
        return view('admin.createpelanggaran');
    }

    public function storepelanggaran(Request $request){
        $validasi = $request->validate([
            'nama_pelanggaran' => 'required',
            'jumlah_poin' => 'required|integer',
            'tindakan_langsung' => 'required',
        ]);

        \App\Models\Pelanggaran::create($validasi);

        return redirect()->route('admin.createpelanggaran')->with('success','Data pelanggaran berhasil ditambah!');
    }

    public function tampilkanpelanggaran(){
        $pelanggarans = \App\Models\Pelanggaran::orderBy('jumlah_poin', 'DESC')->get();
        
        return view('admin.tampilkanpelanggaran', ['pelanggarans' => $pelanggarans]);
    }

    public function editpelanggaran(Pelanggaran $pelanggaran){
        return view('admin.editpelanggaran', ['pelanggaran' => $pelanggaran]);
    }

    public function updatepelanggaran(Request $request){
        $validasi = $request->validate([
            'nama_pelanggaran' => 'required',
            'jumlah_poin' => 'required|integer',
            'tindakan_langsung' => 'required',
        ]);

        $barisTransaksiPelanggaran = TransaksiPelanggaran::where('pelanggaran_id','=',$request->id)->count();

        if($barisTransaksiPelanggaran > 0){
            return redirect()->route('admin.tampilkanpelanggaran')->with('warning','Tidak boleh update pelanggaran yang sudah ada di transaksi pelanggaran');
        }else{
            \App\Models\Pelanggaran::updateOrCreate(
                ['id' => $request->id],
                ['nama_pelanggaran' => $request->nama_pelanggaran,
                'jumlah_poin' => $request->jumlah_poin,
                'tindakan_langsung' => $request->tindakan_langsung]
            );
        }

        return redirect()->route('admin.tampilkanpelanggaran')->with('success','Pelanggaran berhasil di edit!');
    }

    public function destroypelanggaran(Pelanggaran $pelanggaran){
        $barisTransaksiPelanggaran = \App\Models\TransaksiPelanggaran::where('pelanggaran_id','=',$pelanggaran->id)->count();
        
        if($barisTransaksiPelanggaran > 0 ){
            return redirect()->back()->with('warning','Tidak bisa menghapus pelanggaran karena ada sudah digunakan!');
        }else{
            \App\Models\Pelanggaran::where('id', '=', $pelanggaran->id)->delete();
            return redirect()->back()->with('success','Berhasil menghapus pelanggaran!');
        }
    }

    public function destroypengguna(Pengguna $pengguna){
        if($pengguna->level == "admin"){
            return redirect()->back()->with('info','Tidak bisa menghapus admin!');
        }elseif($pengguna->level == "guru"){
            $guru = \App\Models\Guru::where('pengguna_id','=', $pengguna->id)->first();
            $barisTransaksiPelanggaran = \App\Models\TransaksiPelanggaran::where('guru_id','=', $guru->id)->count();

                if($barisTransaksiPelanggaran > 0){
                    return redirect()->back()->with('info','Tidak bisa menghapus guru yang sudah pernah memberi poin untuk siswa!');
                }else{
                    \App\Models\Pengguna::find($pengguna->id)->delete();
                    return redirect()->back()->with('success','Berhasil menghapus Guru!');
                }            

        }elseif($pengguna->level == "siswa"){
            $siswa = \App\Models\Siswa::where('pengguna_id','=', $pengguna->id)->first();
            $barisTransaksiPelanggaran = \App\Models\TransaksiPelanggaran::where('siswa_id','=', $siswa->id)->count();

                if($barisTransaksiPelanggaran > 0){
                    return redirect()->back()->with('info','Tidak bisa menghapus siswa yang sudah pernah diberi poin!');
                }else{
                    \App\Models\Pengguna::find($pengguna->id)->delete();
                    return redirect()->back()->with('success','Berhasil menghapus Siswa!');
                } 
        }else{
            return redirect()->back()->with('info','Ada kesalahan!');
        }




        // $a = \App\Models\Pengguna::find($pengguna->id)->delete();
    }

    public function createpengguna(){
        return view('admin.createpengguna');
    }

    public function storepengguna(Request $request){     
        if($request->level == "admin"){
            $validasiPengguna = $request->validate([
                'username' => 'required|unique:penggunas,username',
                'password' => 'required',
                'level' => 'required'
            ]);
            $tambahPengguna = \App\Models\Pengguna::create($validasiPengguna);
            // return redirect()->route('admin.tampilkanpengguna')->with('success','Berhasil ditambah!');
            return redirect()->back()->with('success','Okeii berhasil ditambah!');

        }elseif($request->level == "guru"){
            $validasiPengguna = $request->validate([
                'username' => 'required|unique:penggunas,username',
                'password' => 'required',
                'level' => 'required'
            ]);
            $validasiGuru = $request->validate([
                'nama' => 'required'
            ]);            
            $tambahPengguna = \App\Models\Pengguna::create($validasiPengguna);
            $pengguna = \App\Models\Pengguna::latest()->first();
            $tambahGuru = \App\Models\Guru::create([
                'pengguna_id' => $pengguna->id,
                'nama' => $request->nama
            ]);
            // return redirect()->route('admin.tampilkanpengguna')->with('success','Berhasil ditambah!');
            return redirect()->back()->with('success','Okeii berhasil ditambah!');            
        }elseif($request->level == "siswa"){
            $validasiPengguna = $request->validate([
                'username' => 'required|unique:penggunas,username',
                'password' => 'required',
                'level' => 'required'
            ]);

            $validasiSiswa = $request->validate([
                'nama' => 'required',
                'kelas' => 'required'
            ]);

            $tambahPengguna = \App\Models\Pengguna::create($validasiPengguna);
            $pengguna = \App\Models\Pengguna::latest()->first();
            $tambahSiswa = \App\Models\Siswa::create([
                'pengguna_id' => $pengguna->id,
                'nama' => $request->nama,
                'kelas' => $request->kelas
            ]);
            // return redirect()->route('admin.tampilkanpengguna')->with('success','Berhasil ditambah!');
            return redirect()->back()->with('success','Okeii berhasil ditambah!');
        }else{
            echo "ada yang salah!";
        }
    }

    public function tampilkanpengguna(){
        $penggunas = \App\Models\Pengguna::with('siswa')->orderBy('level','asc')->get();
        return view('admin.tampilkanpengguna', ['penggunas' => $penggunas]);
    }

    public function editpengguna(Pengguna $pengguna){
        return view('admin.editpengguna', ['pengguna' => $pengguna]);
    }

    public function updatepengguna(Request $request){
    
        if($request->level == "admin"){
            $validasiAdmin = $request->validate([
                'username' => 'required',
                'password' => 'required',
                'level' => 'required',
                'id' => 'required',
            ]);

            Pengguna::updateOrCreate(
                ['id' => $request->id],
                [
                'username' => $request->username,
                'password' => $request->password,
                ]
            );

            return redirect()->route('admin.tampilkanpengguna')->with('success','Pengguna sukses di edit!');
        }elseif($request->level == "guru"){
            $validasiAdmin = $request->validate([
                'password' => 'required',
                'level' => 'required',
                'id' => 'required',
                'nama' => 'required',
            ]);

            Pengguna::updateOrCreate(
                ['id' => $request->id],
                [
                'password' => $request->password,
                ]
            );

            Guru::updateOrCreate(
                ['pengguna_id' => $request->id],
                [
                'nama' => $request->nama,
                ]
            );

            return redirect()->route('admin.tampilkanpengguna')->with('success','Pengguna sukses di edit!');
        }elseif ($request->level == "siswa") {
            $validasiSiswa = $request->validate([
                'password' => 'required',
                'level' => 'required',
                'id' => 'required',
                'nama' => 'required',
                'kelas' => 'required',
            ]);

            Pengguna::updateOrCreate(
                ['id' => $request->id],
                [
                'password' => $request->password,
                ]
            );

            Siswa::updateOrCreate(
                ['pengguna_id' => $request->id],
                [
                'nama' => $request->nama,
                'kelas' => $request->kelas,
                ]
            );

            return redirect()->route('admin.tampilkanpengguna')->with('success','Pengguna sukses di edit!');
        }
    }

    public function siswaperingatan(){
        $poins = \App\Models\Poin::with('siswa')->orderBy('poin', 'desc')->get();
        return view('admin.siswaperingatan', ['poins' => $poins]);
    }

    public function downloadlaporansiswa(TransaksiPelanggaran $transaksipelanggaran){
        $poinSiswa = \App\Models\TransaksiPelanggaran::with('pelanggaran')->where('siswa_id', '=', $transaksipelanggaran->siswa_id)->get();
        $poin = \App\Models\Poin::where('siswa_id', '=', $transaksipelanggaran->siswa_id)->first();
        return view('admin.downloadlaporansiswa', ['poinSiswa' => $poinSiswa, 'poin' => $poin]);
    }

    public function hapustransaksipoin(TransaksiPelanggaran $transaksipelanggaran){
        // get trans. pelanggaran sesuai siswa_id dan pelanggaran_id
        $transaksipelanggaran = \App\Models\TransaksiPelanggaran::where('siswa_id', $transaksipelanggaran->siswa_id)->where('id', $transaksipelanggaran->id)->first();

        // get brp jmlh poin pelanggaran
        $jumlahPoinTransaksiPelanggaran = $transaksipelanggaran->pelanggaran->jumlah_poin;

        // ambil siswa_id
        $siswa_id = $transaksipelanggaran->siswa_id;

        // proses hapus baris transaksi pelanggaran dan kurangi jumlah poin di tbl POIN
        if($jumlahPoinTransaksiPelanggaran != null){
            if($siswa_id != null){

                // get data poin siswa
                $poin = \App\Models\Poin::where('siswa_id',$siswa_id)->first();

                DB::beginTransaction();

                // hapus baris trans. pelanggaran
                $hapus = \App\Models\TransaksiPelanggaran::where('id', $transaksipelanggaran->id)->delete();

                // update poin siswa setelah dihapus trans. pelanggaran
                $updatePoin = \App\Models\Poin::where('siswa_id', $siswa_id)->update([
                    'poin' => $poin->poin - $jumlahPoinTransaksiPelanggaran
                ]);

                if( !$hapus || !$updatePoin ){
                    DB::rollbackTransaction();
                    return redirect()->route('admin.siswaperingatan')->with('info', 'Ada yang salah! Hubungi admin!');
                } else {
                    DB::commit();

                    // cek baris siswa_id di tbl poin
                    $cekPoin = \App\Models\Poin::where('siswa_id', $siswa_id)->first();
                    // hapus jika poinnya 0
                    if($cekPoin->poin == 0){
                        $cekPoin = \App\Models\Poin::where('siswa_id', $siswa_id)->delete();
                        return redirect()->route('admin.siswaperingatan')->with('success', 'Berhasil hapus transaksi poin!');
                    }
                    return redirect()->route('admin.siswaperingatan')->with('success', 'Berhasil hapus transaksi poin!');
                }

                return redirect()->route('admin.siswaperingatan')->with('success', 'Transaksi poin berhasil dihapus!');
            }else{
                return redirect()->route('admin.siswaperingatan')->with('info', 'Ada yang salah! Hubungi admin!');
            }
        }else{
            return redirect()->route('admin.siswaperingatan')->with('info', 'Ada yang salah! Hubungi admin!');
        }       
        
        
        // if($hapus){
        //     return redirect()->back()->with('success', 'Transaksi poin berhasil dihapus!');
        // }else{
        //     return redirect()->back()->with('info', 'Ada yang salah! Hubungi admin!');            
        // }
    }
}
