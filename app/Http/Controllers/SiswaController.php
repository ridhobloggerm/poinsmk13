<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiswaController extends Controller
{
    public function __construct(){
        return $this->middleware('isSiswa');
    }

    public function index(Request $request){
        $pengguna = \App\Models\Pengguna::where('id', '=', $request->session()->get('id'))->first();
        $siswa = \App\Models\Siswa::where('pengguna_id', '=', $request->session()->get('id'))->first();
        $transaksiPelanggarans = \App\Models\TransaksiPelanggaran::where('siswa_id', '=', $siswa->id)->get();
        $belumDitindak = \App\Models\TransaksiPelanggaran::where('siswa_id', '=', $siswa->id)->where('status_tindakan_langsung', 0)->count();
        $sudahDitindak = \App\Models\TransaksiPelanggaran::where('siswa_id', '=', $siswa->id)->where('status_tindakan_langsung', 1)->count();

        
        return view('siswa.index', ['pengguna' => $pengguna, 'siswa' => $siswa, 'transaksiPelanggarans' => $transaksiPelanggarans, 'belumDitindak' => $belumDitindak, 'sudahDitindak' => $sudahDitindak]);
    }
    
    public function rankingpoin(){
        $poins = \App\Models\Poin::orderBy('poin', 'desc')->get();
        return view('siswa.rankingpoin', ['poins' => $poins]);
    }

    public function pelanggaransaya(Request $request){ 
        $pengguna = \App\Models\Pengguna::where('id', '=', $request->session()->get('id'))->first();
        $siswa = \App\Models\Siswa::where('pengguna_id', '=', $request->session()->get('id'))->first();
        $transaksiPelanggarans = \App\Models\TransaksiPelanggaran::with('pelanggaran')->where('siswa_id', '=', $siswa->id)->get();
        
        return view('siswa.pelanggaransaya', ['pengguna' => $pengguna, 'siswa' => $siswa, 'transaksiPelanggarans' => $transaksiPelanggarans]);
    }

    public function pelanggaran(){
        $pelanggarans = \App\Models\Pelanggaran::orderBy('jumlah_poin', 'DESC')->get();
        
        return view('siswa.pelanggaran', ['pelanggarans' => $pelanggarans]);
    }
}
