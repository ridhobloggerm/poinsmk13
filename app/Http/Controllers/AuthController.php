<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pengguna;

class AuthController extends Controller
{
    public function index(){
        return view('auth.masuk');
    }

    public function store(Request $request){
        $validasi = $request->validate([
            'username' => 'required',
            'password' => 'required',
        ]);

        $cekLogin = Pengguna::where('username','=',$request->username)->where('password','=',$request->password)->count();

        if($cekLogin > 0){
            $cekData = Pengguna::where('username','=',$request->username)->where('password','=',$request->password)->first(['id','level']);
            
            $level = $cekData->level;
            $id = $cekData->id;

            if ($level == "guru") {
                $request->session()->put(['level'=> $level, 'id'=> $id]);
                return redirect()->route('guru.index');
            }elseif ($level == "siswa") {
                $request->session()->put(['level'=> $level, 'id'=> $id]);                
                return redirect()->route('siswa.index');                
            }elseif ($level == "admin") {
                $request->session()->put(['level'=> $level, 'id'=> $id]);                
                return redirect()->route('admin.index');  
            }
        }else{
            return redirect()->back()->with('warning','Akun yang dimasukkan tidak terdaftar!');
        }
    }

    public function logout(Request $request){
        $request->session()->forget('level');
        return redirect()->route('auth.index');
    }
}
