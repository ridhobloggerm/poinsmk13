<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\FlareClient\View;

class GuruController extends Controller
{
    public function __construct(){
      $this->middleware('isGuru');
    }  

    public function index(Request $request){
        $id = $request->session()->get('id');
        $pengguna = \App\Models\Pengguna::where('id','=',$id)->first();

        $jumlahBelumTindak = \App\Models\TransaksiPelanggaran::where('guru_id',$pengguna->guru->id)->where('status_tindakan_langsung', 0)->count();

        $jumlahSudahTindak = \App\Models\TransaksiPelanggaran::where('guru_id',$pengguna->guru->id)->where('status_tindakan_langsung', 1)->count();

        $jumlahTransaksi = \App\Models\TransaksiPelanggaran::where('guru_id',$pengguna->guru->id)->count();

        $jumlahSiswaDiberi = \App\Models\TransaksiPelanggaran::where('guru_id', $pengguna->guru->id)->distinct('siswa_id')->count();

        return view('guru.index', ['pengguna' => $pengguna, 'jumlahBelumTindak' => $jumlahBelumTindak, 'jumlahTransaksi' => $jumlahTransaksi, 'jumlahSiswaDiberi' => $jumlahSiswaDiberi, 'jumlahSudahTindak' => $jumlahSudahTindak]);
    }

    public function pilihkelas(){
        return view('guru.pilihkelas');
    }

    public function carisiswa(Request $request){
        $siswaKelass = \App\Models\Siswa::where('nama','LIKE','%'.$request->nama.'%')->get();
        $pelanggarans= \App\Models\Pelanggaran::all();

        // ID GURU
        $guruId = $request->session()->get('id');
        $pengguna = \App\Models\Pengguna::find($guruId);

        return view('guru.hasilcari', ['siswaKelass' => $siswaKelass, 'pelanggarans' => $pelanggarans, 'pengguna' => $pengguna]);
    } 

    public function kelas(Request $request, $kelas){
        $siswaKelass = \App\Models\Siswa::where('kelas','=', $kelas)->get();
        $pelanggarans= \App\Models\Pelanggaran::all();

        // ID GURU
        $guruId = $request->session()->get('id');
        $pengguna = \App\Models\Pengguna::find($guruId);

        return view('guru.kelas', ['siswaKelass' => $siswaKelass, 'kelas' => $kelas, 'pelanggarans' => $pelanggarans, 'pengguna' => $pengguna]);
    }

    public function createpoin(Request $request){

        // ID GURU
        $guruId = $request->session()->get('id');
        $pengguna = \App\Models\Pengguna::find($guruId);

        // ID PELANGGARAN
        $pelanggarans = \App\Models\Pelanggaran::get();

        // SISWA
        $siswas = \App\Models\Siswa::orderByDesc('kelas','asc')->get();

        return view('guru.createpoin', ['pengguna' => $pengguna, 'pelanggarans' => $pelanggarans, 'siswas' => $siswas]);
    }

    public function storepoin(Request $request){
        $validasi = $request->validate([
            'pelanggaran_id' => 'required|integer',
            'siswa_id' => 'required|integer' ,
            'guru_id' => 'required|integer',
            'status_tindakan_langsung' => 'required',
        ]);
        
        $poin = \App\Models\Pelanggaran::where('id','=',$request->pelanggaran_id)->first('jumlah_poin');
        $poinDasar = $poin->jumlah_poin;

        $siswa_id = $request->siswa_id;
        $poinSaatIniSiswa = \App\Models\Poin::where('siswa_id','=',$siswa_id)->first('poin');



        if($poinSaatIniSiswa != null){
            $penambahanPoin = $poinDasar + $poinSaatIniSiswa->poin;
            
            \App\Models\Poin::updateOrCreate(
                ['siswa_id' => $siswa_id],
                ['poin' => $penambahanPoin],
            );

            \App\Models\TransaksiPelanggaran::create($validasi);

            return redirect()->route('guru.pilihkelas')->with('success','Berhasil input poin! Lihat status tindakan langsung di menu riwayat pemberian poin kamu!');
        }else{
            $penambahanPoin = $poinDasar + 0;
            
            \App\Models\Poin::updateOrCreate(
                ['siswa_id' => $siswa_id],
                ['poin' => $penambahanPoin],
            );

            \App\Models\TransaksiPelanggaran::create($validasi);

            return redirect()->route('guru.pilihkelas')->with('success','Berhasil input poin! Lihat status tindakan langsung di menu riwayat pemberian poin kamu!');
        }
    }

    public function lihatpoindiberi(Request $request){
        $pengguna_id = $request->session()->get('id');
        $guru_id = \App\Models\Guru::where('pengguna_id','=',$pengguna_id)->first(); 
        $dataPelanggarans = \App\Models\TransaksiPelanggaran::with('siswa')->with('Pelanggaran')->where('guru_id','=',$guru_id->id)->orderBy('created_at','desc')->get();

        return view('guru.lihatpoindiberi', ['dataPelanggarans' => $dataPelanggarans]);  
    }

    public function updatetindakan(Request $request){
        $updateStatusTindakan = \App\Models\TransaksiPelanggaran::where('id',$request->id)->update([
            'status_tindakan_langsung' => $request->status_tindakan_langsung
        ]);

        if($updateStatusTindakan == 1){
            return redirect()->back()->with('success','Berhasil update status tindakan.');
        }else{
            return redirect()->back()->with('info','Ada yang salah! Hubungi admin!');
        }
    }

    public function lihatpoin(){
        $poins = \App\Models\Poin::orderBy('poin', 'desc')->get();

        return view('guru.lihatpoin', ['poins' => $poins]);
    }

    public function detailpoin(){
        $detailpoins = \App\Models\Pelanggaran::orderBy('jumlah_poin', 'asc')->get();

        return view('guru.detailpoin', ['detailpoins' => $detailpoins]);
    }

    public function multiplecreate(Request $request){
        $pelanggarans= \App\Models\Pelanggaran::all();

        // ID GURU
        $guruId = $request->session()->get('id');
        $pengguna = \App\Models\Pengguna::find($guruId);

        $siswas = \App\Models\Siswa::orderBy('nama','asc')->get();
        return view('guru.multiplecreate', ['siswas' => $siswas, 'pelanggarans' => $pelanggarans, 'pengguna' => $pengguna]);
    }

    public function multiplecreatestore(Request $request){
        $validasi = $request->validate([
            'pelanggaran_id.*' => 'required|integer',
            'siswa_id.*' => 'required|integer',
            'guru_id' => 'required|integer',
            'status_tindakan_langsung' => 'required|integer'
        ]);

        // dd(count($request->siswa_id) - 1);

        // array dari inputan request masukkan ke array untuk diproses ke tabel
        for ($i=0; $i < count($request->siswa_id); $i++) { 
            $data[] = [
                'pelanggaran_id' => $request->pelanggaran_id[$i],
                'siswa_id' => $request->siswa_id[$i],
                'guru_id' => $request->guru_id,
                'status_tindakan_langsung' => $request->status_tindakan_langsung
            ];
            
            // dd($data[$i]['siswa_id']);
            

            //cek poin siswa
            $barisPoinSiswa = \App\Models\Poin::where('siswa_id', $data[$i]['siswa_id'])->first();
            // cek poin di tbl pelanggaran
            $barisPoinPelanggaran = \App\Models\Pelanggaran::where('id', $data[$i]['pelanggaran_id'])->first();

                if($barisPoinSiswa != null){
                    // jika ditemukan ada siswa di tbl poin
                    // tambahkan jmlh poin terakhir dengan poin sesuai pelanggaran sekarang
                    $poinAkhir = $barisPoinSiswa->poin + $barisPoinPelanggaran->jumlah_poin;
                    // beginTransaction
                    // DB::beginTransaction();
                    // update baris poin siswa di tbl poin
                    $updatePoin = \App\Models\Poin::updateOrCreate(
                        ['siswa_id' => $data[$i]['siswa_id']],
                        ['poin' => $poinAkhir]
                    );
                    // tambahkan trans. pelanggaran
                    $insertTransPelanggaran = \App\Models\TransaksiPelanggaran::create($data[$i]);
                    // akhir beginTransaction()
                    // if($updatePoin || $insertTransPelanggaran){
                    //     DB::commit();
                    //     return redirect()->route('guru.multiplecreate')->with('success','Berhasil input poin beberapa siswa!');
                    // }else{
                    //     DB::rollback();
                    //     return redirect()->route('guru.multiplecreate')->with('info','Ada yang salah! Hubungi admin!');
                    // }
                }else{
                    // jika tidak ditemukan siswa di baris tbl poin maka tambahkan saja poin ...
                    // pelanggaran sekarang + 0
                    $poinAkhir = 0 + $barisPoinPelanggaran->jumlah_poin;
                    // beginTransaction
                    // DB::beginTransaction();
                    // update baris poin siswa di tbl poin
                    $updatePoin = \App\Models\Poin::updateOrCreate(
                        ['siswa_id' => $data[$i]['siswa_id']],
                        ['poin' => $poinAkhir]
                    );
                    // tambahkan trans. pelanggaran
                    $insertTransPelanggaran = \App\Models\TransaksiPelanggaran::create($data[$i]);
                    // akhir beginTransaction()
                    // if($updatePoin || $insertTransPelanggaran){
                    //     DB::commit();
                    //     return redirect()->route('guru.multiplecreate')->with('success','Berhasil input poin beberapa siswa!');
                    // }else{
                    //     DB::rollback();
                    //     return redirect()->route('guru.multiplecreate')->with('info','Ada yang salah! Hubungi admin!');
                    // }
                }
            }
            return redirect()->route('guru.multiplecreate')->with('success','Berhasil input poin beberapa siswa!');
    }

    public function pelanggaran(){
        $pelanggarans = \App\Models\Pelanggaran::orderBy('jumlah_poin', 'DESC')->get();
        
        return view('guru.pelanggaran', ['pelanggarans' => $pelanggarans]);
    }
}
