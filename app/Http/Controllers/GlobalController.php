<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class GlobalController extends Controller
{
    public function daftar(){
        return view('global.daftar');
    }

    public function daftarstore(Request $request){
        $validasiPengguna = $request->validate([
            'username' => 'required|unique:penggunas',
            'password' => 'required|max:8| min:5',
            'level' => 'required'
        ]);

        $validasiSiswa = $request->validate([
            'nama' => 'required',
            'kelas' => 'required'
        ]);

        $tambahPengguna = \App\Models\Pengguna::create($validasiPengguna);
        $pengguna = \App\Models\Pengguna::latest()->first();
        $tambahSiswa = \App\Models\Siswa::create([
            'pengguna_id' => $pengguna->id,
            'nama' => $request->nama,
            'kelas' => $request->kelas
        ]);
        return redirect()->route('auth.index')->with('success','Akun berhasil dibuat. Silahkan masuk!');
    }
}
