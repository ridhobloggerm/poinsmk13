<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Siswa extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function Siswa(){
        return $this->hasMany('\App\Models\TransaksiPelanggaran');
    }

    public function Pengguna(){
        return $this->belongsTo('\App\Models\Pengguna');
    }

    public function Poin(){
        return $this->hasOne('\App\Models\Poin');
    }

    public function getNamaAttribute($value){
        return strtoupper($value);
    }

    public function getKelasAttribute($value){
        return strtoupper($value);
    }
}
