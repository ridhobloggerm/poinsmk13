<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pelanggaran extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function TransaksiPelanggaran(){
        return $this->hasMany('\App\Models\TransaksiPelanggaran');
    }

    public function getNamaPelanggaranAttribute($value){
        return ucwords($value);
    }

    public function getTindakanLangsungAttribute($value){
        return ucwords($value);
    }
}
