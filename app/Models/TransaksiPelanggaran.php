<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TransaksiPelanggaran extends Model
{
    use HasFactory;
    protected $guarded = [];

    public function Pelanggaran(){
        return $this->belongsTo('\App\Models\Pelanggaran');
    }

    public function Siswa(){
        return $this->belongsTo('\App\Models\Siswa');
    }
}
