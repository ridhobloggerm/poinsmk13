<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pengguna extends Model
{
    use HasFactory;

    protected $guarded = [];

    public function guru(){
        return $this->hasOne('App\Models\Guru');
    }

    public function siswa(){
        return $this->hasOne('App\Models\Siswa');
    }
}
