<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\GuruController;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\SiswaController;
use App\Http\Controllers\GlobalController;

Route::get('/', function () {
   return view('auth.masuk');
});


// AUTH
Route::prefix('auth')->group(function (){
    Route::get('masuk', [AuthController::class, 'index'])->name('auth.index');
    Route::post('masuk', [AuthController::class, 'store'])->name('auth.store');
    Route::get('keluar', [AuthController::class, 'logout'])->name('auth.logout');
});

// GURU
Route::prefix('guru')->group(function () {
    Route::get('home', [GuruController::class, 'index'])->name('guru.index');
    Route::get('createpoin', [GuruController::class, 'createpoin'])->name('guru.createpoin');
    Route::post('createpoin', [GuruController::class, 'storepoin'])->name('guru.storepoin');
    Route::get('lihatpoindiberi', [GuruController::class, 'lihatpoindiberi'])->name('guru.lihatpoindiberi');
    Route::get('lihatpoin', [GuruController::class, 'lihatpoin'])->name('guru.lihatpoin');
    Route::get('detailpoin', [GuruController::class, 'detailpoin'])->name('guru.detailpoin');
    Route::get('pilihkelas', [GuruController::class, 'pilihkelas'])->name('guru.pilihkelas');
    Route::get('kelas/{kelas}', [GuruController::class, 'kelas'])->name('guru.kelas');
    Route::post('carisiswa', [GuruController::class, 'carisiswa'])->name('guru.carisiswa');
    Route::patch('updatetindakan', [GuruController::class, 'updatetindakan'])->name('guru.updatetindakan');
    Route::get('multiplecreate', [GuruController::class, 'multiplecreate'])->name('guru.multiplecreate');
    Route::post('multiplecreatestore', [GuruController::class, 'multiplecreatestore'])->name('guru.multiplecreatestore');
    Route::get('pelanggaran', [GuruController::class, 'pelanggaran'])->name('guru.pelanggaran');
});

// ADMIN
Route::prefix('admin')->group(function () {
    Route::get('index', [AdminController::class, 'index'])->name('admin.index');
    // pelanggaran
    Route::get('createpelanggaran', [AdminController::class, 'createpelanggaran'])->name('admin.createpelanggaran');
    Route::post('storepelanggaran', [AdminController::class, 'storepelanggaran'])->name('admin.storepelanggaran');
    Route::get('tampilkanpelanggaran', [AdminController::class, 'tampilkanpelanggaran'])->name('admin.tampilkanpelanggaran');
    Route::get('editpelanggaran/{pelanggaran:id}', [AdminController::class, 'editpelanggaran'])->name('admin.editpelanggaran');
    Route::patch('updatepelanggaran', [AdminController::class, 'updatepelanggaran'])->name('admin.updatepelanggaran');
    Route::get('destroypelanggaran/{pelanggaran:id}', [AdminController::class, 'destroypelanggaran'])->name('admin.destroypelanggaran');
    
    // pengguna
    Route::get('createpengguna', [AdminController::class, 'createpengguna'])->name('admin.createpengguna');
    Route::post('storepengguna', [AdminController::class, 'storepengguna'])->name('admin.storepengguna');
    Route::get('tampilkanpengguna', [AdminController::class, 'tampilkanpengguna'])->name('admin.tampilkanpengguna');
    Route::get('editpengguna/{pengguna:id}', [AdminController::class, 'editpengguna'])->name('admin.editpengguna');
    Route::patch('updatepengguna', [AdminController::class, 'updatepengguna'])->name('admin.updatepengguna');
    Route::get('destroypengguna/{pengguna:id}', [AdminController::class, 'destroypengguna'])->name('admin.destroypengguna');

    // LAPORAN
    Route::get('siswaperingatan', [AdminController::class, 'siswaperingatan'])->name('admin.siswaperingatan');
    Route::get('laporan', [AdminController::class, 'laporan'])->name('admin.laporan');
    Route::get('downloadlaporansiswa/{transaksipelanggaran:siswa_id}', [AdminController::class, 'downloadlaporansiswa'])->name('admin.downloadlaporansiswa');
    Route::get('hapustransaksipoin/{transaksipelanggaran:id}', [AdminController::class, 'hapustransaksipoin'])->name('admin.hapustransaksipoin');

    
});

// SISWA
Route::prefix('siswa')->group(function () {
    Route::get('index', [SiswaController::class, 'index'])->name('siswa.index');
    Route::get('pelanggaransaya', [SiswaController::class, 'pelanggaransaya'])->name('siswa.pelanggaransaya');
    Route::get('rankingpoin', [SiswaController::class, 'rankingpoin'])->name('siswa.rankingpoin');
    Route::get('pelanggaran', [SiswaController::class, 'pelanggaran'])->name('siswa.pelanggaran');
});

// GLOBAL
Route::get('daftar', [GlobalController::class, 'daftar'])->name('global.daftar');
Route::post('daftarstore', [GlobalController::class, 'daftarstore'])->name('global.daftarstore');