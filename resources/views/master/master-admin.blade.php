<!doctype html>
<html lang="en">
  <head>
    
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=JetBrains+Mono&display=swap" rel="stylesheet">

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <style>
        button{
            background-color: transparent;
            border-color: transparent;
            padding-top: 5px;
            padding-bottom: 5px;
        }

        button:hover{
            background-color: #F8F4EA !important;
            border-radius: 40px;
        }

        a{
          color: #4E6C50;
          font-weight: bold;
          text-decoration: none;
        }
        a:hover{
          text-decoration: none;
        }
    </style>

     {{-- loader css --}}
     <style> 
      #loader { 
          border: 12px solid #f3f3f3; 
          border-radius: 50%; 
          border-top: 12px solid #444444; 
          width: 70px; 
          height: 70px; 
          animation: spin 1s linear infinite; 
      } 
      
      @keyframes spin { 
          100% { 
              transform: rotate(360deg); 
          } 
      } 
      
      .center { 
          position: absolute; 
          top: 0; 
          bottom: 0; 
          left: 0; 
          right: 0; 
          margin: auto; 
      } 
    </style> 


    <title>POINSMK</title>
  </head>
  {{-- <body style="font-size: 1rem; background-color: #FFD56F;">
    <div class="container-fluid">
      <div class="row fixed-top p-3 border-bottom bg-light">
        <div class="col-1">
            @yield('back')
        </div>
        <div class="col-9">
          <b class="text-secondary">@yield('judul')</b>
        </div>
        <div class="col-2">
            <a href="{{route('auth.logout')}}">
            <img src="/img/profile.png" alt="">
          </a>       
        </div>
      </div>

    @yield('konten')

    <div class="row fixed-bottom border p-3 bg-light" style="padding-top: 200px;">
      <div class="col">
          <a href="{{route('admin.index')}}">
            <center><img src="/img/home.png" width="" alt=""></center>
          </a>
      </div>
    </div>
    </div> 
    
  @include('sweetalert::alert') --}}
  
  {{-- JS --}}
  {{-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script> --}}
  {{-- JS --}}
    {{-- <div class="row" style="margin-bottom: 30%">
      <div class="col">

      </div>
    </div>
  </body>
</html> --}}

<body style="font-family: 'JetBrains Mono', monospace; background-color: #F8F4EA; margin-bottom: 100px;">
  <div id="loader" class="center"></div> 
  <div class="container-fluid p-4">

        {{-- TOP NAV --}}
        <div class="row fixed-top shadow d-lg-none d-print-none" style="background-color: #D2DAFF; border-radius: 10px;">
              @if ($_SERVER['REQUEST_URI'] != "/admin/index")
                <div class="col-2  pt-2 pl-4 p-3 bg-light" style="">
                  @yield('back')
                </div>            
              @endif
              <div class="col pt-2 p-3 pl-4">
                <b style="color: #00425A;"> @yield('judul') </b>
              </div>
              @if ($_SERVER['REQUEST_URI'] == "/admin/index")
              <div class="col-2 p-2">
                <button>                  
                    <a href="{{route('auth.logout')}}">
                        <img src="/img/global/profil.svg" alt="">
                    </a>
                </button>  
              </div>         
              @endif              
        </div>

        @yield('konten')

        {{-- MENU--}}
        <div class="row fixed-bottom p-2 d-lg-none d-print-none" style="background-color: #00425A; border-radius: 10px;">
           <div class="col">
              <center>  
                <button>                            
                  <a href="{{route('admin.index')}}">
                    <img src="/img/global/home.svg" alt="">
                   </a>                            
                </button>
              </center>
        </div>       

  </div> 

@include('sweetalert::alert')
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>

<script>
  AOS.init();
</script>

<script>
  history.pushState(null, null, window.location.href);
  history.back();
  window.onpopstate = () => history.forward();
</script>

{{-- LOADING JS --}}
<script> 
  document.onreadystatechange = function() { 
      if (document.readyState !== "complete") { 
          document.querySelector( 
          "body").style.visibility = "hidden"; 
          document.querySelector( 
          "#loader").style.visibility = "visible"; 
      } else { 
          document.querySelector( 
          "#loader").style.display = "none"; 
          document.querySelector( 
          "body").style.visibility = "visible"; 
      } 
  }; 
</script>


</body>
</html>