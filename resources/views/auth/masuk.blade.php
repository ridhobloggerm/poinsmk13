<!DOCTYPE html>
<html lang="en">
    <head>    
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">          
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">    
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">    
        <title>POINSMK</title>

        <style>
            a:hover{
                color:#B6EADA;
                font-weight: bold;
            }

            .btn:hover{
                background-color:#FEC868;
                border-color: none;
            }
        </style>
      </head>
<body>
    <div class="container-fluid">

        <div class="row  pb-4" style="margin-top: 10%;">
            <div class="col">
                <center>
                <span style="font-size: 3rem; color: #FC7300">E-Poin SMK13</span> <br>
                <div class="text-secondary pt-2"><i>"Kembangkan budaya, majukan teknologi!"</i></div>
                </center>
            </div>
        </div>

        <div class="row fixed-bottom p-4" style="margin-bottom: 10%;">
            <div class="col">
                <div class="row">
                    <div class="col">
                        
                        
                            @error('username')
                                <div class="alert alert-danger" role="alert">
                                    {{$message}}
                                </div>
                            @enderror 
                            @error('password')
                                <div class="alert alert-danger" role="alert">
                                    {{$message}}
                                </div>
                            @enderror    
                        

                        <form method="POST" action="{{route('auth.store')}}">
                            @csrf

                            <div class="mb-3">
                                <label class="text-secondary">Nama Unik</label>
                                <input type="text" class="form-control p-3" name="username" value="{{old('username')}}">
                            </div>
                            <div class="mb-3">
                                <label class="text-secondary">Kata Sandi</label>
                                <input type="password" class="form-control p-3" name="password" value="{{old('password')}}">
                            </div>
                            <button type="submit" class="btn w-100 btn-primary">Masuk</button>
                          </form>
                          <br>
                          <span class="text-secondary">Butuh bantuan? <a href="https://wa.me/6282381807580">Klik Disini!</a></span> <br>
                          <span class="text-secondary">Belum punya akun? <a href="{{route('global.daftar')}}">Daftar Disini!</a></span>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('sweetalert::alert')
</body>
</html>