{{-- @section('konten')
<div class="row" style="margin-top: 60px;">
    <div class="col">
        <table class="table">
            <thead>
              <tr class="text-secondary">
                <th>Nama</th>
                <th>Kelas</th>
                <th>Total Poin</th>
              </tr>
            </thead>
            <tbody class="bg-light">
                @foreach ($poins as $poin)
                    <tr class="text-secondary">
                        <td>{{$poin->siswa->nama}}</td>
                        <td>{{$poin->siswa->kelas}}</td>
                        <td>{{$poin->poin}}</td>
                    </tr>
                @endforeach
            </tbody>
          </table>
    </div>
</div>
@endsection --}}

@extends('master.master')
@section('back')
    <a href="{{route('guru.index')}}">
        <img src="/img/back.png" alt="">
    </a>
@endsection
@section('judul')
    Ranking Poin
@endsection

@section('konten')

<div class="container-fluid p-0"  style="margin-top: 40px; border-radius: 10px;">
    <div class="row" style="background-color: #00425A; color: white; border-radius: 10px;">
        <div class="col p-2"><center> Nama </center></div>
        <div class="col p-2"><center>Kelas</center></div>
        <div class="col-2 p-2"><center>Poin</div>
    </div>
@foreach ($poins as $poin)
<div class="row p-2 border" style="background-color: white; border-radius: 10px;">
    <div class="col p-2">
        {{$poin->siswa->nama}}
    </div>
    <div class="col p-2">        
        <center>{{$poin->siswa->kelas}}</center>
    </div>
    <div class="col-2 p-2">
       <center> {{$poin->poin}} </center>
    </div>
</div>
@endforeach
@endsection