@extends('master.master')
@section('judul')
    Hai, {{$pengguna->guru->nama}}! 
@endsection
@section('konten')
<div class="row mt-3">
    <div class="col">
          <div class="row text-light" style="margin-top: 50px;">
              <div class="col p-3 mr-1" data-aos="fade-up" style="background-color: #2C74B3; border-radius: 10px;">
                  <div>
                        <img src="/img/global/user.svg" alt="">
                  </div>
                  <div>
                        <center>  <span style="font-size: 3rem"> {{$jumlahSiswaDiberi}} </span> </center>
                  </div>
                  <div>
                        <center> Siswa diberi poin </center>
                  </div>
              </div>
              <div class="col p-3 mr-1" data-aos="fade-down" style="background-color: #4E6C50; border-radius: 10px;">
                <div>
                      <img src="/img/global/input.svg" alt="">
                </div>
                <div>
                      <center>  <span style="font-size: 3rem"> {{$jumlahTransaksi}} </span> </center>
                </div>
                <div>
                      <center> Transaksi Poin </center>
                </div>
            </div>
          </div>

          <div class="row mt-2 text-light">
            <div class="col p-3 ml-1" data-aos="fade-up" style="background-color: #00425A; border-radius: 10px;">
                <div>
                      <img src="/img/global/student.svg" alt="">
                </div>
                <div>
                      <center>  <span style="font-size: 3rem"> {{$jumlahSudahTindak}} </span> </center>
                </div>
                <div>
                      <center> Sudah Ditindak </center>
                </div>
            </div>
            <div class="col p-3 ml-1" data-aos="fade-down" style="background-color: #400E32; border-radius: 10px;">
              <div>
                    <img src="/img/global/belum.svg" alt="">
              </div>
              <div>
                    <center>  <span style="font-size: 3rem"> {{$jumlahBelumTindak}} </span> </center>
              </div>
              <div>
                    <center> Belum ditindak </center>
              </div>
          </div>
        </div>
    </div>
</div>


<div class="row mt-3 p-3" data-aos="flip-up" style="background-color: #FFFFFF; border-radius: 10px;">
    <div class="col-2 p-2" style="background-color: #D9D9D9; border-radius: 10px;">
        <center> <img style="height: 30px;" src="/img/global/list.svg" alt=""> </center>
    </div>
    <div class="col">
        <button style="width: 100%; text-align: left;">
            <a href="{{route('guru.pelanggaran')}}"> Detail Pelanggaran </a>
        </button>
    </div>
</div>

<div class="row mt-3 p-3" data-aos="flip-up" style="background-color: #FFFFFF; border-radius: 10px;">
    <div class="col-2 p-2" style="background-color: #D9D9D9; border-radius: 10px;">
        <center> <img style="height: 30px;" src="/img/global/list.svg" alt=""> </center>
    </div>
    <div class="col">
        <button style="width: 100%; text-align: left;">
            <a href="{{route('guru.multiplecreate')}}"> Input Poin Siswa </a>
        </button>
    </div>
</div>

<div class="row mt-3 p-3" data-aos="flip-up" style="background-color: #FFFFFF; border-radius: 10px;">
    <div class="col-2 p-2" style="background-color: #D9D9D9; border-radius: 10px;">
        <center> <img style="height: 30px;" src="/img/global/list.svg" alt=""> </center>
    </div>
    <div class="col">
        <button style="width: 100%; text-align: left;">
            <a href="{{route('guru.lihatpoindiberi')}}"> Riwayat Pemberian Poin</a>
        </button>
    </div>
</div>

<div class="row mt-3 p-3" data-aos="flip-up" style="background-color: #FFFFFF; border-radius: 10px; margin-bottom: 100px;">
    <div class="col-2 p-2" style="background-color: #D9D9D9; border-radius: 10px;">
        <center> <img style="height: 30px;" src="/img/global/list.svg" alt=""> </center>
    </div>
    <div class="col">
        <button style="width: 100%; text-align: left;">
            <a href="{{route('guru.lihatpoin')}}"> Ranking Poin Siswa </a>
        </button>
    </div>
</div>
@endsection