@extends('master.master')
@section('back')
    <a href="{{route('guru.index')}}">
        <img src="/img/back.png" alt="">
    </a>
@endsection
@section('judul')
    Poin Yang Pernah Di Input
@endsection

@section('konten')

<div class="container-fluid p-0"  style="margin-top: 40px; border-radius: 10px;">
    <div class="row" style="background-color: #00425A; color: white; border-radius: 10px;">
        <div class="col p-2"><center> Nama </center></div>
        <div class="col p-2"><center>Pelanggaran</center></div>
        <div class="col-2 p-2"><center>Poin</div>
    </div>
@foreach ($dataPelanggarans as $dataPelanggaran)
<div class="row p-2 border {{ $dataPelanggaran->status_tindakan_langsung == "0" ? "text-danger" : "text-dark" }}" style="background-color: white; border-radius: 10px;"  data-toggle="collapse" data-target="#{{$dataPelanggaran->id}}" aria-expanded="true">
    <div class="col p-2">
        {{$dataPelanggaran->siswa->nama}}
    </div>
    <div class="col p-2">        
        <center>{{$dataPelanggaran->Pelanggaran->nama_pelanggaran}}</center>
    </div>
    <div class="col-2 p-2">
       <center> {{$dataPelanggaran->Pelanggaran->jumlah_poin}} </center>
    </div>
</div>
<div class="collapse" id="{{$dataPelanggaran->id}}">
    <div class="card card-body" style="background-color: #00425A; color: white;">
        - {{$dataPelanggaran->siswa->kelas}} <br>
        - {{ $dataPelanggaran->created_at != null ? date_format($dataPelanggaran->created_at,"d M Y H:i:s") : "-"}}
        @if ($dataPelanggaran->status_tindakan_langsung == "0")
        <br> <br>
            <form action="{{route('guru.updatetindakan')}}" method="POST">
                @csrf
                @method('patch')
                <br>
                - {{$dataPelanggaran->Pelanggaran->tindakan_langsung}}

                <input type="hidden" value="{{$dataPelanggaran->id}}" name="id">
                <input type="hidden" name="status_tindakan_langsung" value="1">
                <input class="btn btn-success w-100" type="submit" value="Konfirmasi sudah ditindak">
            </form>
        @elseif ($dataPelanggaran->status_tindakan_langsung == "1")
        <br> <br>
            <form action="{{route('guru.updatetindakan')}}" method="POST">
                @csrf
                @method('patch')

                <input type="hidden" value="{{$dataPelanggaran->id}}" name="id">
                <input type="hidden" name="status_tindakan_langsung" value="0">
                <input class="btn btn-danger w-100" type="submit" value="Ubah status ke belum ditindak">
            </form>
        @endif
    </div>
</div>
@endforeach
</div>
@endsection