<html lang="en">
<head>
  <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  

  <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
    
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=JetBrains+Mono&display=swap" rel="stylesheet">

    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>

    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">

    <style>
        button{
            background-color: transparent;
            border-color: transparent;
            padding-top: 5px;
            padding-bottom: 5px;
        }

        button:hover{
            background-color: #F8F4EA !important;
            border-radius: 40px;
        }

        a{
          color: #4E6C50;
          font-weight: bold;
          text-decoration: none;
        }
        a:hover{
          text-decoration: none;
        }
    </style>

        {{-- loader css --}}
        <style> 
          #loader { 
              border: 12px solid #f3f3f3; 
              border-radius: 50%; 
              border-top: 12px solid #444444; 
              width: 70px; 
              height: 70px; 
              animation: spin 1s linear infinite; 
          } 
          
          @keyframes spin { 
              100% { 
                  transform: rotate(360deg); 
              } 
          } 
          
          .center { 
              position: absolute; 
              top: 0; 
              bottom: 0; 
              left: 0; 
              right: 0; 
              margin: auto; 
          } 
        </style> 
</head>

<body style="font-family: 'JetBrains Mono', monospace; background-color: #F8F4EA; margin-bottom: 100px;">
  <div id="loader" class="center"></div> 

    <div class="container-fluid">
    {{-- TOP NAV --}}
    <div class="row fixed-top shadow d-lg-none d-print-none" style="background-color: #D2DAFF; border-radius: 10px;">
        @if ($_SERVER['REQUEST_URI'] != "/guru/home")
          <div class="col-2  pt-2 pl-4 p-3 bg-light" style="">
          <center>  
            <a href="{{route('guru.index')}}">
                <img src="/img/back.png" alt="">
            </a>  
          </center>
          </div>            
        @endif
        <div class="col pt-2 p-3 pl-4">
          <b style="color: #00425A;"> Input Multiple Poin </b>
        </div>
        @if ($_SERVER['REQUEST_URI'] == "/guru/home")
        <div class="col-2 p-2">
          <button>                  
              <a href="{{route('auth.logout')}}">
                  <img src="/img/global/profil.svg" alt="">
              </a>
          </button>  
        </div>         
        @endif              
    </div>

        <div class="row p-4" style="margin-top: 15%;">
            <div class="col">

                @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
                @endif

                <form action="{{route('guru.multiplecreatestore')}}" method="POST">
                @csrf

                <button class="btn btn-success btn-sm add-more float-right" type="button">Tambah Siswa</button> <br> <br>

                <div class="control-group after-add-more">
                    <label>Siswa</label>
                    <select class="form-control" name="siswa_id[]" required>
                        <option value="" selected></option>
                        @foreach ($siswas as $siswa)
                            <option value="{{$siswa->id}}">{{$siswa->nama}} / {{$siswa->kelas}}</option>
                        @endforeach
                    </select> <br>

                    <label>Pelanggaran</label>
                    <select class="form-control" name="pelanggaran_id[]" required>
                        <option value="" selected></option>
                        @foreach ($pelanggarans as $pelanggaran)
                            <option value="{{$pelanggaran->id}}">{{$pelanggaran->nama_pelanggaran}}  - {{$pelanggaran->jumlah_poin}} Poin</option>
                        @endforeach
                    </select>
                    
                    <input type="hidden" name="guru_id" value="{{$pengguna->guru->id}}">
                    <input type="hidden" name="status_tindakan_langsung" value="0">
                    <hr>
                </div>
                
                {{-- <button class="btn btn-dark text-light w-100" type="submit">Tambah Semua</button>  --}}
                {{-- tombol modal --}}
                <button type="button" class="btn btn-info w-100" data-toggle="modal" data-target="#myModal">Input Semua</button>             
                
                {{-- modal dari tombol --}}
                <div id="myModal" class="modal fade" role="dialog">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                      </div>
                      <div class="modal-body">
                        Jika salah input, hubungi admin untuk di reset!  <br> <br>
                        <p>Yakin akan menginput semua pelanggaran sekaligus?</p>
                      </div>
                      <div class="modal-footer">
                        <button class="btn btn-danger text-light w-100" type="submit">Yakin dan lanjutkan!</button> 
                      </div>
                    </div>
                  </div>
                </div>

                </form>

                <div class="copy invisible">
                    <div class="control-group">
                        <label>Siswa</label>
                        <select class="form-control" name="siswa_id[]" required>
                            <option value="" selected></option>
                            @foreach ($siswas as $siswa)
                                <option value="{{$siswa->id}}">{{$siswa->nama}} / {{$siswa->kelas}}</option>
                            @endforeach
                        </select> <br>
    
                        <label>Pelanggaran</label>
                        <select class="form-control" name="pelanggaran_id[]" required>
                            <option value="" selected></option>
                            @foreach ($pelanggarans as $pelanggaran)
                                <option value="{{$pelanggaran->id}}">{{$pelanggaran->nama_pelanggaran}} - {{$pelanggaran->jumlah_poin}} Poin</option>
                            @endforeach
                        </select>

                        <input type="hidden" name="guru_id" value="{{$pengguna->guru->id}}">
                        <input type="hidden" name="status_tindakan_langsung" value="0">
                      <br>
                      <button class="btn btn-danger btn-sm remove" type="button">Hapus</button>
                      <hr>
                    </div>
                </div>
            </div>
        </div>               
        

        {{-- MENU--}}
        <div class="row fixed-bottom p-2 d-lg-none d-print-none" style="background-color: #00425A; border-radius: 10px;">
            <div class="col">
               <center>  
                 <button>                            
                   <a href="{{route('guru.index')}}">
                     <img src="/img/home.png" height="25px;" alt="">
                    </a>                            
                 </button>
               </center>
             </div>
             @if ($_SERVER['REQUEST_URI'] != "/guru/home")
            <div class="col">
               <center>  
                 <button>                            
                   <a href="{{route('guru.pilihkelas')}}">
                     <img src="/img/create.png" height="30px;" alt="">
                    </a>                            
                 </button>
               </center>
             </div>
            <div class="col">
               <center>  
                 <button>                            
                   <a href="{{route('guru.lihatpoindiberi')}}">
                     <img src="/img/diberi.png" height="30px;" alt="">
                    </a>                            
                 </button>
               </center>
             </div>
            <div class="col">
               <center>  
                 <button>                            
                   <a href="{{route('guru.lihatpoin')}}">
                     <img src="/img/poin.png" height="25px;" alt="">
                    </a>                            
                 </button>
               </center>
             </div>
             @endif
             {{-- <div class="col">
                 <a href="{{$_SERVER['REQUEST_URI']}}">Reload</a>
             </div> --}}
         </div>       

        </div>

@include('sweetalert::alert')


<script>
    history.pushState(null, null, window.location.href);
    history.back();
    window.onpopstate = () => history.forward();
</script>

{{-- LOADING JS --}}
<script> 
  document.onreadystatechange = function() { 
      if (document.readyState !== "complete") { 
          document.querySelector( 
          "body").style.visibility = "hidden"; 
          document.querySelector( 
          "#loader").style.visibility = "visible"; 
      } else { 
          document.querySelector( 
          "#loader").style.display = "none"; 
          document.querySelector( 
          "body").style.visibility = "visible"; 
      } 
  }; 
</script>


{{-- js modal --}}
<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.1/dist/jquery.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-Fy6S3B9q64WdZWQUiU+q4/2Lc9npb8tCaSX9FK7E8HnRr0Jz8D6OP9dO5Vg3Q9ct" crossorigin="anonymous"></script>
{{-- js modal --}}

<script type="text/javascript">
    $(document).ready(function() {
      $(".add-more").click(function(){ 
          var html = $(".copy").html();
          $(".after-add-more").after(html);
      });

      
      $("body").on("click",".remove",function(){ 
          $(this).parents(".control-group").remove();
      });
    });
</script>

</body>
</html>
