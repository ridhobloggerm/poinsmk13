@extends('master.master')
@section('back')
    <a href="{{route('guru.pilihkelas')}}">
        <img src="/img/back.png" alt="">
    </a>
@endsection
@section('judul')
    Hasil Pencarian
@endsection

@section('konten')
    <div class="row p-2" style="margin-top: 60px;">
        <div class="col">
            @foreach ($siswaKelass as $siswaKelas)
            <a class="btn border p-2 w-100" style="background-color: white; border-radius: 10px;" data-toggle="collapse" href="#{{$siswaKelas->id}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                {{$siswaKelas->nama}} / {{$siswaKelas->kelas}}
              </a>
            </p>
            <div class="collapse" id="{{$siswaKelas->id}}">
              <div class="card card-body">
               
                <form action="{{route('guru.storepoin')}}" method="post">
                    @csrf

                    <label class="text-danger">Pelanggaran</label>
                    <select class="form-control" name="pelanggaran_id">
                        @foreach ($pelanggarans as $pelanggaran)
                            <option value="{{$pelanggaran->id}}">{{$pelanggaran->nama_pelanggaran}} - {{$pelanggaran->jumlah_poin}} Poin</option>
                        @endforeach
                    </select>

                    <input type="hidden" name="siswa_id" value="{{$siswaKelas->id}}">
                    <input type="hidden" name="guru_id" value="{{$pengguna->guru->id}}">
                    <input type="hidden" name="status_tindakan_langsung" value="0">

                    {{-- modal --}}

                        <button style="margin-top: 10px;" type="button" class="btn btn-danger btn-md w-100" data-toggle="modal" data-target="#myModal{{$siswaKelas->id}}"> Input Poin</button>

                        <div id="myModal{{$siswaKelas->id}}" class="modal fade" role="dialog">
                            <div class="modal-dialog">
                                <!-- konten modal-->
                                <div class="modal-content">
                                    <!-- heading modal -->
                                    {{-- <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Bagian heading modal</h4>
                                    </div> --}}
                                    <!-- body modal -->
                                    <div class="modal-body">
                                        Yakin akan menambahkan poin yang sudah diinput? <br>
                                    </div>
                                    <!-- footer modal -->
                                    <div class="modal-footer">
                                        <input class="form-control bg-danger text-light mt-3" type="submit" value="Yakin, lanjutkan input!">
                                    </div>
                                </div>
                            </div>
                        </div>

                    {{-- end modal --}}

                </form>

              </div>
            </div>
            @endforeach
        </div>
    </div>
@endsection