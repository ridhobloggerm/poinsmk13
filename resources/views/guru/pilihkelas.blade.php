@extends('master.master')
@section('back')
    <a href="{{route('guru.index')}}">
        <img src="/img/back.png" alt="">
    </a>
@endsection
@section('judul')
    Pilih Kelas
@endsection

@section('konten')
    <div class="row">
        <div class="col">
        <form action="{{route('guru.carisiswa')}}" method="post">
            @csrf

            <div class="row" style="margin-top: 80px">
                <div class="col-9">
                    <input style="border-radius: 15px;border-color: #F55050" class="form-control" type="text" name="nama" placeholder="Ketik nama siswa . . .">         
                </div>
                <div class="col-3">
                    <input style="border-radius: 15px; background-color: #F55050;" class="form-control text-light" type="submit" value="Cari"> 
                </div>
            </div>
        </form>
        </div>
    </div>

    <div class="row">
        <div class="col pt-3">
            <center class="text-secondary"> Atau pilih berdasarkan kelas!</center>
        </div>
    </div>

    <div class="row border bg-light p-2 m-1 mt-3" style="border-radius: 5px;margin-bottom:">
        <div class="col-12 p-2 border-bottom">
            <img src="/img/fish.png"  height="50rem; alt="">
            <a class="text-secondary" style="text-decoration: none" href="{{route('guru.kelas', "xapat")}}"> X APAT </a>
        </div>
        <div class="col-12 p-2  border-bottom">
            <img src="/img/fish.png"  height="50rem; alt="">
            <a class="text-secondary" style="text-decoration: none" href="{{route('guru.kelas', "xiapat")}}"> XI APAT </a>
        </div>
        <div class="col-12 p-2">
            <img src="/img/fish.png"  height="50rem; alt="">
            <a class="text-secondary" style="text-decoration: none" href="{{route('guru.kelas', "xiiapat")}}"> XII APAT </a>
        </div>
    </div>

    <div class="row border bg-light p-2 m-1" style="border-radius: 5px;">
        <div class="col-12 p-2 border-bottom">
            <img src="/img/tree.png"  height="50rem; alt="">
            <a class="text-secondary" style="text-decoration: none" href="{{route('guru.kelas', "xatph")}}"> X ATPH </a>
        </div>
        <div class="col-12 p-2  border-bottom">
            <img src="/img/tree.png"  height="50rem; alt="">
            <a class="text-secondary" style="text-decoration: none" href="{{route('guru.kelas', "xiatph")}}"> XI ATPH </a>
        </div>
        <div class="col-12 p-2">
            <img src="/img/tree.png"  height="50rem; alt="">
            <a class="text-secondary" style="text-decoration: none" href="{{route('guru.kelas', "xiiatph")}}"> XII ATPH </a>
        </div>
    </div>

    <div class="row border bg-light p-2 m-1" style="border-radius: 5px;">
        <div class="col-12 p-2 border-bottom">
            <img src="/img/computer.png"  height="50rem; alt="">
            <a class="text-secondary" style="text-decoration: none" href="{{route('guru.kelas', "xtkj")}}"> X TKJ </a>
        </div>
        <div class="col-12 p-2  border-bottom">
            <img src="/img/computer.png"  height="50rem; alt="">
            <a class="text-secondary" style="text-decoration: none" href="{{route('guru.kelas', "xitkj")}}"> XI TKJ </a>
        </div>
        <div class="col-12 p-2">
            <img src="/img/computer.png"  height="50rem; alt="">
            <a class="text-secondary" style="text-decoration: none" href="{{route('guru.kelas', "xiitkj")}}"> XII TKJ </a>
        </div>
    </div>

    <div class="row border bg-light p-2 m-1" style="border-radius: 5px;">
        <div class="col-12 p-2 border-bottom">
            <img src="/img/motor.png"  height="50rem; alt="">
            <a class="text-secondary" style="text-decoration: none" href="{{route('guru.kelas', "xtbsm")}}"> X TBSM </a>
        </div>
        <div class="col-12 p-2  border-bottom">
            <img src="/img/motor.png"  height="50rem; alt="">
            <a class="text-secondary" style="text-decoration: none" href="{{route('guru.kelas', "xitbsm")}}"> XI TBSM </a>
        </div>
        <div class="col-12 p-2">
            <img src="/img/motor.png"  height="50rem; alt="">
            <a class="text-secondary" style="text-decoration: none" href="{{route('guru.kelas', "xiitbsm")}}"> XII TBSM </a>
        </div>
    </div>
@endsection