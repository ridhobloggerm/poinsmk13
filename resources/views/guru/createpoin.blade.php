@extends('master.master')

@section('konten')
   <form action="{{route('guru.storepoin')}}" method="POST">
      @csrf
      
      <select name="siswa_id">
         @foreach ($siswas as $siswa)
            <option value="{{$siswa->id}}">{{$siswa->kelas}} - {{$siswa->nama}}</option>  
         @endforeach
      </select> <br>

      <select name="pelanggaran_id">
         @foreach ($pelanggarans as $pelanggaran)
            <option value="{{$pelanggaran->id}}">{{$pelanggaran->nama_pelanggaran}}</option>  
         @endforeach
      </select> <br>

      {{$pengguna->guru->id}}
      <input type="hidden" name="guru_id" value="{{$pengguna->guru->id}}">
      <input type="hidden" name="status_tindakan_langsung" value="0">

      <input type="submit" value="TAMBAHKAN"> 
   </form>
@endsection