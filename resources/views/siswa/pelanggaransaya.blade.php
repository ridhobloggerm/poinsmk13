@extends('master.master-siswa')
@section('back')
<center>
<a href="{{route('siswa.index')}}">
    <img src="/img/back.png" alt="">
</a>
</center>
@endsection
@section('judul','Pelanggaran Saya')
@section('konten')


    <div class="row" style="margin-top: 10%;">
        <div class="col p-3 rounded border">
            <table class="table text-secondary">
                <thead>
                    <tr>
                        <th>No</th>
                        <th>Pelanggaran</th>
                        <th>Poin</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($transaksiPelanggarans as $transaksiPelanggaran)
                        <tr class="{{$transaksiPelanggaran->status_tindakan_langsung == "0" ? "text-danger" : "text-dark"}}" data-toggle="collapse" data-target="#{{$transaksiPelanggaran->id}}" aria-expanded="false">
                            <td>{{$loop->iteration}}.</td>
                            <td> {{$transaksiPelanggaran->pelanggaran->nama_pelanggaran}} </td>
                            <td> {{$transaksiPelanggaran->pelanggaran->jumlah_poin}}  </td>
                        </tr>
                        <tr class="collapse bg-secondary text-light" id="{{$transaksiPelanggaran->id}}">
                            <td colspan="4">
                                {{ $transaksiPelanggaran->created_at != null ? date_format($transaksiPelanggaran->created_at,"d M Y H:i:s") : "-"}} <br>
                                @if ($transaksiPelanggaran->status_tindakan_langsung == "0")                                
                                   <b class="text-warning">{{$transaksiPelanggaran->pelanggaran->tindakan_langsung}} </b>
                                @endif
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

    

@endsection