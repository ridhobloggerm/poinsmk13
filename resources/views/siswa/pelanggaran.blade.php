@extends('master.master-siswa')

@section('back')   
<a href="{{route('siswa.index')}}">
   <center><img src="/img/global/back.svg" alt=""></center>
</a>
@endsection
@section('judul')
   Semua Pelanggaran
@endsection

@section('konten')

<div class="container bg-light" style="margin-top: 50px; border-radius: 10px;">
<div class="row p-1 pt-2">
    <div class="col-2 mr-1 p-2" style="border-radius: 10px; background-color: #00425A; color: white;"><center> No. </center></div>
    <div class="col mr-1 p-2" style="border-radius: 10px; background-color: #00425A; color: white;"><center> Nama </center></div>
</div>
@foreach ($pelanggarans as $pelanggaran )
    <div class="row p-1">
        <div class="col-2 mr-1 p-2" style="border-radius: 10px; background-color: #F8F4EA;">
            <center>{{$loop->iteration}}.</center>
        </div>
        <div class="col mr-1 p-2" style="border-radius: 10px; background-color: #F8F4EA;">
            <b> {{$pelanggaran->nama_pelanggaran}} </b> <br>
            <small> {{$pelanggaran->tindakan_langsung}} </small> <br>

            <small class="text-danger"> {{$pelanggaran->jumlah_poin}} Poin </small>
        </div>
    </div>
@endforeach
</div>

@endsection