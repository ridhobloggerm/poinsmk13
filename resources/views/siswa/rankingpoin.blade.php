@extends('master.master-siswa')
@section('back')
<center>
<a href="{{route('siswa.index')}}">
    <img src="/img/back.png" alt="">
</a>
</center>
@endsection
@section('judul','Ranking Poin Siswa')

@section('konten')

<div class="container-fluid p-0"  style="margin-top: 40px; border-radius: 10px;">
    <div class="row" style="background-color: #00425A; color: white; border-radius: 10px;">
        <div class="col p-2"><center> Nama </center></div>
        <div class="col p-2"><center>Kelas</center></div>
        <div class="col-2 p-2"><center>Poin</div>
    </div>
@foreach ($poins as $poin)
<div class="row p-2 border" style="background-color: white; border-radius: 10px;">
    <div class="col p-2">
        {{$poin->siswa->nama}}
    </div>
    <div class="col p-2">        
        <center>{{$poin->siswa->kelas}}</center>
    </div>
    <div class="col-2 p-2">
       <center> {{$poin->poin}} </center>
    </div>
</div>
@endforeach

@endsection