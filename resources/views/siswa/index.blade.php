@extends('master.master-siswa')

@section('back')

@section('judul')

Hai, {{$pengguna->siswa->nama}}!

{{-- <small> {{$pengguna->siswa->kelas}} </small> --}}

@endsection

@section('konten')



    <div class="row p-4 bg-warning" style="margin-top: 15%; border-radius: 10px; border-top-left-radius: 20px; border-bottom-right-radius: 20px;background-image: url('/img/global/wave.svg'); background-repeat: no-repeat; background-size: cover;">

        <div class="col-2">

            <img src="/img/global/level.svg" alt="" height="30px;">

        </div>

        <div class="col">

            <ceter>

            <span style="font-size: px;"> Level  </span> <br>

            <span style="font-size: 25px;">

                @if (is_null($siswa->poin))

                

                @elseif ($siswa->poin->poin == 0)

                    Anak Baik

                @elseif ($siswa->poin->poin >=1 and  $siswa->poin->poin <= 99)

                    Siswa Nakal

                @elseif ($siswa->poin->poin >=100 and  $siswa->poin->poin <= 499)

                Luar biasa nakal

                @elseif ($siswa->poin->poin >=500 and  $siswa->poin->poin <= 999)

                Siap-siap DO

                @elseif ($siswa->poin->poin >= 1000)

                Sudah DO

                @else
                Masih aman
                @endif

            </span>

            </center>

        </div>

    </div>



    <div class="row p-3">

        <div class="col">

            <div class="row text-light">

                <div class="col mr-1 shadow bg-danger border p-3" style="text-align: center; border-radius: 10px;background-image: url('/img/global/wave1.svg'); background-repeat: no-repeat; background-size: cover;" >

                    <span></span> <br>

                    <span style="font-size: 3rem; font-weight: bold;">

                        @if (is_null($siswa->poin))
                            0
                        @elseif ($siswa->poin->poin >= 0)

                            {{$siswa->poin->poin}}

                        @else
                            0
                        @endif

                    </span> <br>

                    <span>Poin</span>

                </div>

                <div class="col ml-1 shadow bg-info border p-3" style="text-align: center; border-radius: 10px;background-image: url('/img/global/wave2.svg'); background-repeat: no-repeat; background-size: cover;" >

                    <span></span> <br>

                    <span style="font-size: 3rem; font-weight: bold;">

                        @if (is_null($transaksiPelanggarans))

                            0

                        @elseif ($transaksiPelanggarans->count() >= 0)

                            {{$transaksiPelanggarans->count()}}

                        @endif

                    </span> <br>

                    <span>Pelanggaran</span>

                </div>

            </div>



            <div class="row mt-1 text-light">

                <div class="col mr-1 shadow bg-warning border p-3" style="text-align: center; border-radius: 10px;background-image: url('/img/global/wave3.svg'); background-repeat: no-repeat; background-size: cover;" >

                    <span></span> <br>

                    <span style="font-size: 3rem; font-weight: bold;">

                        @if (is_null($belumDitindak))

                            0

                        @elseif ($belumDitindak >= 0)

                            {{$belumDitindak}}

                        @endif

                    </span> <br>

                    <span>Belum dilakukan</span>

                </div>

                <div class="col ml-1 shadow bg-primary border p-3" style="text-align: center; border-radius: 10px;background-image: url('/img/global/wave4.svg'); background-repeat: no-repeat; background-size: cover;" >

                    <span></span> <br>

                    <span style="font-size: 3rem; font-weight: bold;">

                        @if (is_null($sudahDitindak))

                            0

                        @elseif ($sudahDitindak >= 0)

                            {{$sudahDitindak}}

                        @endif

                    </span> <br>

                    <span>Sudah dilakukan</span>

                </div>

            </div>

        </div>

    </div>




    <div class="row mt-1 p-3" data-aos="flip-up" style="background-color: #FFFFFF; border-radius: 10px;">

        <div class="col-2 p-2" style="background-color: #D9D9D9; border-radius: 10px;">

            <center> <img style="height: 30px;" src="/img/global/list.svg" alt=""> </center>

        </div>  

        <div class="col">

            <button style="width: 100%; text-align: left;">

                <a href="{{route('siswa.pelanggaran')}}"> Detail Pelanggaran</a>

            </button>

        </div>

    </div>

    <div class="row mt-3 p-3" data-aos="flip-up" style="background-color: #FFFFFF; border-radius: 10px;">

        <div class="col-2 p-2" style="background-color: #D9D9D9; border-radius: 10px;">

            <center> <img style="height: 30px;" src="/img/global/list.svg" alt=""> </center>

        </div>

        <div class="col">

            <button style="width: 100%; text-align: left;">

                <a href="{{route('siswa.pelanggaransaya')}}"> Pelanggaran Saya</a>

            </button>

        </div>

    </div>

    <div class="row mt-3 p-3" data-aos="flip-up" style="background-color: #FFFFFF; border-radius: 10px;">

        <div class="col-2 p-2" style="background-color: #D9D9D9; border-radius: 10px;">

            <center> <img style="height: 30px;" src="/img/global/list.svg" alt=""> </center>

        </div>

        <div class="col">

            <button style="width: 100%; text-align: left;">

                <a href="{{route('siswa.rankingpoin')}}"> Ranking Poin</a>

            </button>

        </div>

    </div>



@endsection