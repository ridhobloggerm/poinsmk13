@extends('master.master-admin')
@section('back')   
<a href="{{route('admin.index')}}">
   <center><img src="/img/global/back.svg" alt=""></center>
</a>
@endsection
@section('judul', 'Daftar Poin Siswa')


@section('konten')
    <div class="row" style="margin-top: 50px; ">
        <div class="col">
            <table class="table">
                <thead>
                  <tr>
                    <th scope="col">No.</th>
                    <th scope="col"><center> Nama </center></th>
                    <th class="d-none d-lg-block" scope="col">Kelas</th>
                    <th scope="col">Poin</th>
                    <th class="d-lg-none" scope="col">Aksi</th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($poins as $poin)
                        
                    @if ($poin->poin > 500)
                        <tr  class="bg-danger text-light">
                            <th scope="row">{{$loop->iteration}}</th>
                            <td> {{$poin->siswa->nama}} </td>
                            <td class="d-none d-lg-block"> {{$poin->siswa->kelas}} </td>
                            <td> <span >{{$poin->poin}}</span> </td>
                            <td class="d-lg-none"> <a class="text-light" style="font-weight: bold; background-color: green" href="{{route('admin.downloadlaporansiswa', $poin->siswa_id)}}">Detail</a> </td>
                        </tr>                                
                    @elseif ($poin->poin >= 250)
                        <tr  class="bg-warning text-light">
                            <th scope="row">{{$loop->iteration}}</th>
                            <td> {{$poin->siswa->nama}} </td>
                            <td class="d-none d-lg-block"> {{$poin->siswa->kelas}} </td>
                            <td> <span >{{$poin->poin}}</span> </td>
                            <td class="d-lg-none"> <a class="text-light" style="font-weight: bold; background-color: green" href="{{route('admin.downloadlaporansiswa', $poin->siswa_id)}}">Detail</a> </td>
                        </tr> 
                    @elseif ($poin->poin >= 0)
                        <tr  class="bg-info text-light">
                            <th scope="row">{{$loop->iteration}}</th>
                            <td> {{$poin->siswa->nama}} </td>
                            <td class="d-none d-lg-block"> {{$poin->siswa->kelas}} </td>
                            <td> <span >{{$poin->poin}}</span> </td>
                            <td class="d-lg-none"> <a class="text-light" style="font-weight: bold; background-color: green" href="{{route('admin.downloadlaporansiswa', $poin->siswa_id)}}">Detail</a> </td>
                        </tr>
                    @endif                        
                </tr>                        
            @endforeach
        </tbody>
                </tbody>
              </table>
        </div>
        
    </div>
@endsection