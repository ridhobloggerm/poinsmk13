@extends('master.master-admin')
@section('back')   
<a href="{{route('admin.index')}}">
   <center><img src="/img/global/back.svg" alt=""></center>
</a>
@endsection
@section('judul') 
   Semua Pengguna
@endsection

@section('konten')

<div class="container-fluid bg-light" style="margin-top: 50px; border-radius: 10px;">
<div class="row p-1 pt-2">
    <div class="col-2 mr-1 p-2" style="border-radius: 10px; background-color: #00425A; color: white;"><center> No. </center></div>
    <div class="col mr-1 p-2" style="border-radius: 10px; background-color: #00425A; color: white;"><center> Keterangan </center></div>
    <div class="col-2 mr-1 p-2" style="border-radius: 10px; background-color: #00425A; color: white;"><center> Aksi </center></div>
</div>
@foreach ($penggunas as $pengguna )
    <div class="row p-1">
        <div class="col-2 mr-1 p-2" style="border-radius: 10px; background-color: #F8F4EA;">
            <center>{{$loop->iteration}}.</center>
        </div>
        <div class="col mr-1 p-2" style="border-radius: 10px; background-color: #F8F4EA;">
            {{$pengguna->username}} <br>
            <small class="text-info">
                @if (is_null($pengguna->siswa))                            
                    
                @else
                    {{$pengguna->siswa->nama}}
                @endif 
                
                <br>
                <button class="bg-warning text-light" style="border-radius: 10px;"> {{$pengguna->level}} </button>
                    @if (is_null($pengguna->siswa))                            
                    
                    @else
                    <button class="bg-primary text-light" style="border-radius: 10px;">
                        {{$pengguna->siswa->kelas}}
                    </button>
                    @endif                
            </small>
        </div>
        <div class="col-2 mr-1 p-2" style="border-radius: 10px; background-color: #F8F4EA;">
            <a href="{{route('admin.editpengguna', $pengguna->id)}}">
                <img src="/img/global/detail.svg" alt="">
            </a>
            <a href="{{route('admin.destroypengguna', $pengguna->id)}}">
                <img src="/img/global/hapus.svg" alt="">
            </a>
        </div>
    </div>
@endforeach
</div>

@endsection