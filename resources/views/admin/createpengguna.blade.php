@extends('master.master-admin')

{{-- @section('konten')
<div class="row" style="margin-top: 20%;">
   <div class="col">
      <form action="{{route('admin.storepengguna')}}" method="POST">
         @csrf
   
         <input type="text" name="username" value="{{old('username')}}" placeholder="usename"> <br>
         <input type="text" name="password" value="{{old('password')}}" placeholder="password"> <br>
         <select name="level">
           <option value="admin">Admin</option>
           <option value="guru">Guru</option>
           <option value="siswa">Siswa</option>
         </select>
         <hr>
         <input type="text" name="nama" value="{{old('nama')}}" placeholder="nama"> <br>
         <select name="kelas">
            <option value="xapat">X APAT</option>
            <option value="xiapat">XI APAT</option>
            <option value="xiiapat">XII APAT</option>
            <option value="xatph">X ATPH</option>
            <option value="xiatph">XI ATPH</option>
            <option value="xiiatph">XII ATPH</option>
            <option value="xtkj">X TKJ</option>
            <option value="xitkj">XI TKJ</option>
            <option value="xiitkj">XII TKJ </option>
            <option value="xtbsm">X TBSM</option>
            <option value="xitbsm">XI TBSM</option>
            <option value="xiitbsm">XII TBSM </option>
          </select>
   
         <input type="submit" value="TAMBAH Pengguna">
      </form>
   </div>
</div>
@endsection --}}


@section('back')   
<a href="{{route('admin.index')}}">
   <center><img src="/img/global/back.svg" alt=""></center>
</a>
@endsection
@section('judul')
   Tambah Pengguna
@endsection
@section('konten')
   <div class="row bg-light p-2 pb-3" style="margin-top: 15%; border-radius: 10px;">
      <div class="col">

         @if ($errors->any())
            <div class="alert alert-danger">
               <ul>
                  @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                  @endforeach
               </ul>
            </div>
         @endif
 
         
         <form action="{{route('admin.storepengguna')}}" method="POST">
            @csrf
            <div class="mb-3">
              <label class="form-label" style="color: #2C74B3">Nama Unik</label>
              <input type="text" value="{{old('username')}}" name="username" class="form-control" style="border-radius: 10px;">
              <div class="form-text" style="color: #2C74B3">Buatlah nama unik tanpa spasi.</div>
            </div>
            <div class="mb-3">
              <label class="form-label" style="color: #2C74B3">Kata Sandi</label>
              <input type="text" value="{{old('password')}}" name="password" class="form-control" style="border-radius: 10px;">
              <div class="form-text" style="color: #2C74B3">Min. 5 dan maks. 8.</div>
            </div>
            <div class="mb-3">
              <label class="form-label" style="color: #2C74B3">Nama Lengkap Sesuai Ijazah</label>
              <input type="text" value="{{old('nama')}}" name="nama" class="form-control" style="border-radius: 10px;">
              <div class="form-text" style="color: #2C74B3">Isi sesuai ijazah.</div>
            </div>
            <div class="mb-3">
              <label class="form-label" style="color: #2C74B3">Level Peran</label>
              <select class="form-control" name="level">
               <option value="admin">Admin</option>
               <option value="guru">Guru</option>
               <option value="siswa">Siswa</option>
             </select>
              <div class="form-text" style="color: #2C74B3">Level di sistem.</div>
            </div>
            <div class="mb-3">
              <label class="form-label" style="color: #2C74B3">Kelas</label>
              <select name="kelas" class="form-control">
               <option value="xapat">X APAT</option>
               <option value="xiapat">XI APAT</option>
               <option value="xiiapat">XII APAT</option>
               <option value="xatph">X ATPH</option>
               <option value="xiatph">XI ATPH</option>
               <option value="xiiatph">XII ATPH</option>
               <option value="xtkj">X TKJ</option>
               <option value="xitkj">XI TKJ</option>
               <option value="xiitkj">XII TKJ </option>
               <option value="xtbsm">X TBSM</option>
               <option value="xitbsm">XI TBSM</option>
               <option value="xiitbsm">XII TBSM </option>
             </select>
              <div class="form-text" style="color: red">Pilih kelas hanya jika level peran yang dipilih adalah siswa.</div>
            </div>
            <input style="border-radius: 10px; margin-top: 30px; background-color: #2C74B3" type="submit" class="btn btn-primary w-100" value="Tambah">
          </form>

      </div>
   </div>
@endsection