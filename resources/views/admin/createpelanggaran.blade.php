@extends('master.master-admin')

{{-- @section('konten')
   <div class="row" style="margin-top: 50%;">
      <div class="col">
         <form action="{{route('admin.storepelanggaran')}}" method="POST">
            @csrf
            
            @error('nama_pelanggaran')
               {{$message}}
            @enderror <br>
            @error('jumlah_poin')
               {{$message}}
            @enderror
            <input type="text" name="nama_pelanggaran" value="{{old('nama_pelanggaran')}}" placeholder="Nama Pelanggaran">
            <input type="number" name="jumlah_poin" value="{{old('jumlah_poin')}}" placeholder="Angka Poin">
      
            <input type="submit" value="TAMBAH PELANGGARAN">
         </form>
      </div>
   </div>
@endsection --}}
@section('back')   
<a href="{{route('admin.index')}}">
   <center><img src="/img/global/back.svg" alt=""></center>
</a>
@endsection
@section('judul')
   Tambah Pelanggaran
@endsection
@section('konten')
   <div class="row bg-light p-2 pb-3" style="margin-top: 15%; border-radius: 10px;">
      <div class="col">

         @if ($errors->any())
            <div class="alert alert-danger">
               <ul>
                  @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                  @endforeach
               </ul>
            </div>
         @endif
 
         
         <form action="{{route('admin.storepelanggaran')}}" method="POST">
            @csrf
            <div class="mb-3">
              <label class="form-label" style="color: #2C74B3">Nama Pelanggaran</label>
              <input type="text" value="{{old('nama_pelanggaran')}}" name="nama_pelanggaran" class="form-control" style="border-radius: 10px;">
              <div class="form-text" style="color: red">Buatlah nama pelanggaran yang sesuai.</div>
            </div>
            <div class="mb-3">
              <label class="form-label" style="color: #2C74B3">Jumlah Poin</label>
              <input type="number" value="{{old('jumlah_poin')}}" name="jumlah_poin" class="form-control" style="border-radius: 10px;">
              <div class="form-text" style="color: red">Masukkan jumlah poin sesuai tata tertib.</div>
            </div>
            <div class="mb-3">
              <label class="form-label" style="color: #2C74B3">Tindakan Langsung</label>
              <input type="text" value="{{old('tindakan_langsung')}}" name="tindakan_langsung" class="form-control" style="border-radius: 10px;">
              <div class="form-text" style="color: red">Tindakan langsung yang harus dilakukan siswa jika melakukan pelanggaran ini.</div>
            </div>
            <input style="border-radius: 10px; margin-top: 30px; background-color: #2C74B3" type="submit" class="btn btn-primary w-100" value="Tambah">
          </form>

      </div>
   </div>
@endsection