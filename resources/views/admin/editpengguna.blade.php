@extends('master.master-admin')
{{-- @section('konten')
<div style="margin-top: 30%;">
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif
    <form action="{{route('admin.updatepengguna')}}" method="POST">
            @csrf
            @method('patch')
        
            <input type="text" name="username" value="{{$pengguna->username}}" readonly>
            <input type="text" name="password" value="{{$pengguna->password}}">
            <input type="hidden" name="id" value="{{$pengguna->id}}">
            <input type="hidden" name="level" value="{{$pengguna->level}}">

            
            @if ($pengguna->level == "guru")
                <input type="text" name="nama" value="{{$pengguna->guru->nama}}">
            @elseif ($pengguna->level == "siswa")
                <input type="text" name="nama" value="{{$pengguna->siswa->nama}}">
                <select name="kelas">
                    <option value="xapat">X APAT</option>
            <option value="xiapat">XI APAT</option>
            <option value="xiiapat">XII APAT</option>
            <option value="xatph">X ATPH</option>
            <option value="xiatph">XI ATPH</option>
            <option value="xiiatph">XII ATPH</option>
            <option value="xtkj">X TKJ</option>
            <option value="xitkj">XI TKJ</option>
            <option value="xiitkj">XII TKJ </option>
            <option value="xtbsm">X TBSM</option>
            <option value="xitbsm">XI TBSM</option>
            <option value="xiitbsm">XII TBSM </option>
                </select>
            @endif
            
            <input type="submit" value="UPDATE">
    </form>
</div> 
@endsection  --}}

@section('back')   
<a href="{{route('admin.tampilkanpengguna')}}">
   <center><img src="/img/global/back.svg" alt=""></center>
</a>
@endsection
@section('judul')
   Edit Pengguna
@endsection
@section('konten')
   <div class="row bg-light p-2 pb-3" style="margin-top: 15%; border-radius: 10px;">
      <div class="col">

         @if ($errors->any())
            <div class="alert alert-danger">
               <ul>
                  @foreach ($errors->all() as $error)
                     <li>{{ $error }}</li>
                  @endforeach
               </ul>
            </div>
         @endif
 
         
         <form action="{{route('admin.updatepengguna')}}" method="POST">
            @csrf
            @method('patch')

            <div class="mb-3">
              <label class="form-label" style="color: #2C74B3">Nama Unik</label>
              <input type="text" value="{{$pengguna->username}}" readonly name="username" class="form-control" style="border-radius: 10px;">
              <div class="form-text" style="color: #2C74B3">Buatlah nama unik tanpa spasi.</div>
            </div>
            <div class="mb-3">
              <label class="form-label" style="color: #2C74B3">Kata Sandi</label>
              <input type="text" value="{{$pengguna->password}}" name="password" class="form-control" style="border-radius: 10px;">
              <div class="form-text" style="color: #2C74B3">Min. 5 dan maks. 8.</div>
            </div>

            @if ($pengguna->level == "guru")
                <label class="form-label" style="color: #2C74B3">Nama Lengkap Sesuai Ijazah</label>
                <input type="text" value="{{$pengguna->guru->nama}}" name="nama" class="form-control" style="border-radius: 10px;">
                <div class="form-text" style="color: #2C74B3">Isi sesuai ijazah.</div>
            @elseif ($pengguna->level == "siswa")
                <label class="form-label" style="color: #2C74B3">Nama Lengkap Sesuai Ijazah</label>
                <input type="text" value="{{$pengguna->siswa->nama}}" name="nama" class="form-control" style="border-radius: 10px;">
                <div class="form-text" style="color: #2C74B3">Isi sesuai ijazah.</div>
                <select class="form-control" name="kelas">
                    <option value="xapat">X APAT</option>
                    <option value="xiapat">XI APAT</option>
                    <option value="xiiapat">XII APAT</option>
                    <option value="xatph">X ATPH</option>
                    <option value="xiatph">XI ATPH</option>
                    <option value="xiiatph">XII ATPH</option>
                    <option value="xtkj">X TKJ</option>
                    <option value="xitkj">XI TKJ</option>
                    <option value="xiitkj">XII TKJ </option>
                    <option value="xtbsm">X TBSM</option>
                    <option value="xitbsm">XI TBSM</option>
                    <option value="xiitbsm">XII TBSM </option>
                </select>
            @endif  

            <input type="hidden" name="id" value="{{$pengguna->id}}">
            <input type="hidden" name="level" value="{{$pengguna->level}}">                        
            <input style="border-radius: 10px; margin-top: 30px; background-color: #2C74B3" type="submit" class="btn btn-primary w-100" value="Edit">
          </form>

      </div>
   </div>
@endsection