@extends('master.master-admin')
@section('back')   
<a href="{{route('admin.siswaperingatan')}}">
   <center><img src="/img/global/back.svg" alt=""></center>
</a>
@endsection
@section('judul', 'Laporan Poin Siswa')
@section('konten')
    <div class="row" style="margin-top: 15%;">
        <div class="col">
            <table border="0">
                <thead>
                  <tr>
                    <td>Nama</th>
                    <th>: {{$poinSiswa[0]->siswa->nama}}</td>
                  </tr>
                  <tr>
                    <td>Kelas</th>
                    <th>: {{$poinSiswa[0]->siswa->kelas}}</td>
                  </tr>
                  <tr>
                    <td>Total Poin</th>
                    <th>: {{$poin->poin}} </td>
                  </tr>
            </table>

            <table class="table" style="margin-top: 10%;">
                <thead> 
                    <tr>
                        <th>No</th>
                        <th>Pelanggaran</th>
                        <th>Poin</th>
                        <th>Tanggal</th>
                    </tr>
                </thead>
                <tbody>
                        @foreach ($poinSiswa as $poin)
                        <tr data-toggle="collapse" data-target="#collapseMalasngoding{{$loop->iteration}}" aria-expanded="false" aria-controls="collapseMalasngoding">                          
                            <th>{{$loop->iteration}}</th>
                            <td>{{$poin->pelanggaran->nama_pelanggaran}}</td>
                            <td>{{$poin->pelanggaran->jumlah_poin}}</td>
                            <td>{{date_format($poin->pelanggaran->created_at, 'd M Y')}}</td>                            
                        </tr>
                        <tr class="collapse bg-secondary" id="collapseMalasngoding{{$loop->iteration}}">
                            <td colspan="4">           
                                    <span class="text-light">{{date_format($poin->pelanggaran->created_at, 'd M Y H:i:s')}} </span> <br> <br>
                                    <button type="button" class="bg-danger text-light rounded" data-toggle="modal" data-target="#modal{{$loop->iteration}}">Hapus</button>

                                    <div id="modal{{$loop->iteration}}" class="modal fade" role="dialog">
                                        <div class="modal-dialog">
                                            <div class="modal-content">
                                                <div class="modal-body">
                                                    Tindakan ini tidak dapat dipulihkan! Yakin untuk menghapus?
                                                </div>
                                                <div class="modal-footer">
                                                    <a class="btn btn-danger text-light" href="{{route('admin.hapustransaksipoin', $poin->id)}}">Yakin dan hapus!</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </form>
                            </td>
                        </tr>
                        
                            
                
                        {{-- <tr>
                            <th>{{$loop->iteration}}</th>
                            <td>{{$poin->pelanggaran->nama_pelanggaran}}</td>
                            <td>{{$poin->pelanggaran->jumlah_poin}}</td>
                            <td>{{date_format($poin->pelanggaran->created_at, 'd M Y')}}</td>
                        </tr> --}}
                        @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection