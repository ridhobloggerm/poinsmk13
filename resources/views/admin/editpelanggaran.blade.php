@extends('master.master-admin')

{{-- @section('konten')
 

<div class="row" style="margin-top: 30%;">
    <div class="col">
@if ($errors->any())
<div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif

   <form action="{{route('admin.updatepelanggaran')}}" method="POST">
      @csrf
      @method('patch')

      <input type="text" name="nama_pelanggaran" value="{{$pelanggaran->nama_pelanggaran}}">
      <input type="number" name="jumlah_poin" value="{{$pelanggaran->jumlah_poin}}">
      <input type="hidden" name="id" value="{{$pelanggaran->id}}">

      <input type="submit" value="UPDATE">
   </form>
    </div>
</div>
@endsection --}}

@section('back')   
<a href="{{route('admin.tampilkanpelanggaran')}}">
   <center><img src="/img/global/back.svg" alt=""></center>
</a>
@endsection
@section('judul')
   Edit Pelanggaran
@endsection
@section('konten')
<div class="row bg-light p-2 pb-3" style="margin-top: 15%; border-radius: 10px;">
    <div class="col">

@if ($errors->any())
<div class="alert alert-danger">
   <ul>
      @foreach ($errors->all() as $error)
         <li>{{ $error }}</li>
      @endforeach
   </ul>
</div>
@endif


<form action="{{route('admin.updatepelanggaran')}}" method="POST">
@csrf
@method('patch')
<div class="mb-3">
  <label class="form-label" style="color: #2C74B3">Nama Pelanggaran</label>
  <input type="text" value="{{$pelanggaran->nama_pelanggaran}}" name="nama_pelanggaran" class="form-control" style="border-radius: 10px;">
  <div class="form-text" style="color: red">Nama pelanggaran ini sebelumnya "{{$pelanggaran->nama_pelanggaran}}"</div>
</div>
<div class="mb-3">
  <label class="form-label" style="color: #2C74B3">Jumlah Poin</label>
  <input type="number" value="{{$pelanggaran->jumlah_poin}}" name="jumlah_poin" class="form-control" style="border-radius: 10px;">
  <div class="form-text" style="color: red">Jumlah poin pelanggaran ini sebelumnya "{{$pelanggaran->jumlah_poin}} poin"</div>
</div>
<div class="mb-3">
  <label class="form-label" style="color: #2C74B3">Tindakan Langsung</label>
  <input type="text" value="{{$pelanggaran->tindakan_langsung}}" name="tindakan_langsung" class="form-control" style="border-radius: 10px;">
  <div class="form-text" style="color: red">Tindakan langsung sebelumnya adalah "{{$pelanggaran->tindakan_langsung}}"</div>
</div>
<input type="hidden" name="id" value="{{$pelanggaran->id}}">
<input style="border-radius: 10px; margin-top: 30px; background-color: #2C74B3" type="submit" class="btn btn-primary w-100" value="Edit">
</form>

</div>
</div>
@endsection