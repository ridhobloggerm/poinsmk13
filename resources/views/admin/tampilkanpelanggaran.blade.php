@extends('master.master-admin')

{{-- @section('konten')
   <div class="row" style="margin-top: 50%;">
      <div class="col">
        <table border="1">
            <tr>
                <td>Nama</td>
                <td>Poin</td>
                <td>Aksi</td>
            </tr>
            @foreach ($pelanggarans as $pelanggaran )
                <tr>
                    <td>{{$pelanggaran->nama_pelanggaran}}</td>
                    <td>{{$pelanggaran->jumlah_poin}}</td>
                    <td> <a href="{{route('admin.editpelanggaran', $pelanggaran->id)}}">Edit</a> </td>
                    <td> <a href="{{route('admin.destroypelanggaran', $pelanggaran->id)}}">Hapus</a> </td>
                </tr>             
            @endforeach
        </table>
      </div>
   </div>
@endsection --}}

@section('back')   
<a href="{{route('admin.index')}}">
   <center><img src="/img/global/back.svg" alt=""></center>
</a>
@endsection
@section('judul')
   Semua Pelanggaran
@endsection

@section('konten')

<div class="container bg-light" style="margin-top: 50px; border-radius: 10px;">
<div class="row p-1 pt-2">
    <div class="col-2 mr-1 p-2" style="border-radius: 10px; background-color: #00425A; color: white;"><center> No. </center></div>
    <div class="col mr-1 p-2" style="border-radius: 10px; background-color: #00425A; color: white;"><center> Nama </center></div>
    <div class="col-2 mr-1 p-2" style="border-radius: 10px; background-color: #00425A; color: white;"><center> Aksi </center></div>
</div>
@foreach ($pelanggarans as $pelanggaran )
    <div class="row p-1">
        <div class="col-2 mr-1 p-2" style="border-radius: 10px; background-color: #F8F4EA;">
            <center>{{$loop->iteration}}.</center>
        </div>
        <div class="col mr-1 p-2" style="border-radius: 10px; background-color: #F8F4EA;">
            <b> {{$pelanggaran->nama_pelanggaran}} </b> <br>
            <small> {{$pelanggaran->tindakan_langsung}} </small> <br>

            <small class="text-danger"> {{$pelanggaran->jumlah_poin}} Poin </small>
        </div>
        <div class="col-2 p-2" style="border-radius: 10px; background-color: #F8F4EA;">
            <a href="{{route('admin.editpelanggaran', $pelanggaran->id)}}">
                <img src="/img/global/detail.svg" alt="">
            </a>
            <a href="{{route('admin.destroypelanggaran', $pelanggaran->id)}}">
                <img src="/img/global/hapus.svg" alt="">
            </a>
        </div>
    </div>
@endforeach
</div>

@endsection