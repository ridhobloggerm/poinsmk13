@extends('master.master-admin')

@section('judul', 'Hai, Admin!')
@section('konten')
<div class="row mt-3">
    <div class="col">
          <div class="row text-light" style="margin-top: 50px;">
              <div class="col p-3 mr-1" data-aos="fade-up" style="background-color: #2C74B3; border-radius: 10px;">
                  <div>
                        <img src="/img/global/user.svg" alt="">
                  </div>
                  <div>
                        <center>  <span style="font-size: 3rem"> {{$pengguna}} </span> </center>
                  </div>
                  <div>
                        <center> Pengguna </center>
                  </div>
              </div>
              <div class="col p-3 ml-1" data-aos="fade-up" style="background-color: #00425A; border-radius: 10px;">
                <div>
                      <img src="/img/global/student.svg" alt="">
                </div>
                <div>
                      <center>  <span style="font-size: 3rem"> {{$guru}} </span> </center>
                </div>
                <div>
                      <center> Guru </center>
                </div>
            </div>
          </div>

          <div class="row mt-2 text-light">
            <div class="col p-3 mr-1" data-aos="fade-down" style="background-color: #4E6C50; border-radius: 10px;">
                <div>
                      <img src="/img/global/student1.svg" alt="">
                </div>
                <div>
                      <center>  <span style="font-size: 3rem"> {{$siswa}} </span> </center>
                </div>
                <div>
                      <center> Siswa </center>
                </div>
            </div>
            <div class="col p-3 ml-1" data-aos="fade-down" style="background-color: #400E32; border-radius: 10px;">
              <div>
                    <img src="/img/global/room.svg" alt="">
              </div>
              <div>
                    <center>  <span style="font-size: 3rem"> 12 </span> </center>
              </div>
              <div>
                    <center> Rombel </center>
              </div>
          </div>
        </div>
    </div>
</div>

<div class="row mt-3 p-3" data-aos="flip-up" style="background-color: #FFFFFF; border-radius: 10px;">
    <div class="col-2 p-2" style="background-color: #D9D9D9; border-radius: 10px;">
        <center> <img style="margin-top: 40%; height: 30px;" src="/img/global/list.svg" alt=""> </center>
    </div>
    <div class="col">
        <button style="width: 100%; text-align: left;">
            <a href="{{route('admin.createpelanggaran')}}"> Buat Pelanggaran</a>
        </button> <br>
        <button style="width: 100%; text-align: left;">
            <a href="{{route('admin.tampilkanpelanggaran')}}"> Detail Pelanggaran</a>
        </button>
    </div>
</div>

<div class="row mt-3 p-3" data-aos="flip-up" style="background-color: #FFFFFF; border-radius: 10px;">
    <div class="col-2 p-2" style="background-color: #D9D9D9; border-radius: 10px;">
        <center> <img style="margin-top: 40%; height: 30px;" src="/img/global/list.svg" alt=""> </center>
    </div>
    <div class="col">
        <button style="width: 100%; text-align: left;">
            <a href="{{route('admin.createpengguna')}}"> Buat Pengguna</a>
        </button> <br>
        <button style="width: 100%; text-align: left;">
            <a href="{{route('admin.tampilkanpengguna')}}"> Semua Pengguna </a>
        </button>
    </div>
</div>

<div class="row mt-3 p-3" data-aos="flip-up" style="background-color: #FFFFFF; border-radius: 10px; margin-bottom: 100px;">
    <div class="col-2 p-2" style="background-color: #D9D9D9; border-radius: 10px;">
        <center> <img style="height: 30px;" src="/img/global/list.svg" alt=""> </center>
    </div>
    <div class="col">
        <button style="width: 100%; text-align: left;">
            <a href="{{route('admin.siswaperingatan')}}"> Laporan siswa </a>
        </button>
    </div>
</div>
@endsection