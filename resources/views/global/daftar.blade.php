<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daftar Epoin SMK13</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    
</head>
<body>
    
    <div class="row mt-4 p-4">
        <div class="col">

            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif

            <form action="{{route('global.daftarstore')}}" method="POST">
                @csrf

                <div class="mb-3">
                  <label class="form-label">Nama Unik</label>
                  <input type="text" class="form-control" name="username"  value="{{old('username')}}">
                  <div  class="form-text">Tidak boleh menggunakan spasi.</div>
                </div>
                <div class="mb-3">
                  <label class="form-label">Kata Sandi</label>
                  <input type="text" class="form-control" name="password" value="{{old('password')}}">
                  <div  class="form-text">Minimal 5 dan maksimal 8 karakter.</div>
                </div>
                <div class="mb-3">
                  <label class="form-label">Nama Lengkap</label>
                  <input type="text" class="form-control" name="nama" value="{{old('nama')}}">
                  <div  class="form-text">Isi sesuai nama asli.</div>
                </div>
                <div class="mb-3">
                  <label class="form-label">Kelas</label>
                  <select name="kelas" class="form-select">
                    <option value="xapat">X APAT</option>
                    <option value="xiapat">XI APAT</option>
                    <option value="xiiapat">XII APAT</option>
                    <option value="xatph">X ATPH</option>
                    <option value="xiatph">XI ATPH</option>
                    <option value="xiiatph">XII ATPH</option>
                    <option value="xtkj">X TKJ</option>
                    <option value="xitkj">XI TKJ</option>
                    <option value="xiitkj">XII TKJ </option>
                    <option value="xtbsm">X TBSM</option>
                    <option value="xitbsm">XI TBSM</option>
                    <option value="xiitbsm">XII TBSM </option>
                  </select>
                  <div  class="form-text">Pilih kelas yang sesuai.</div>
                </div>

                <input type="hidden" name="level" value="siswa">

                <input type="submit" class="btn btn-primary w-100" value="Daftar">
              </form>

              <div class="mt-4">
                Sudah punya akun?  <a style="font-weight: bold;" href="{{route('auth.index')}}">Masuk</a>
              </div>
        </div>
    </div>

</body>
</html>



